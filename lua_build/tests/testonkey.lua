local test = require("lib/gambiarra")
local OnKey = require("controller/onkey")
test("Onkey general", function()
  local ref = {
    x = 1
  }
  local sub = OnKey()
  sub:add({
    a = function()
      ref.x = 2
    end
  })
  sub:run("b")
  eqok(ref.x, 1, "Does not trigger on wrong key")
  sub:run("a")
  eqok(ref.x, 2, "Does trigger on right key")
  sub:add({
    b = function()
      ref.x = 3
    end
  })
  sub:run("b")
  eqok(ref.x, 3, "Adding second works")
  sub:add({
    b = function()
      ref.x = 4
    end
  })
  sub:run("b")
  eqok(ref.x, 4, "Overriding")
  sub:run("a")
  return eqok(ref.x, 2, "Overriding does not disturb others")
end)
return test("Separation", function()
  local ref = {
    x = 1
  }
  local sub1 = OnKey()
  local sub2 = OnKey()
  sub1:add({
    a = function()
      ref.x = 2
    end
  })
  sub2:run("a")
  return eqok(ref.x, 1, "OnKeys are separate")
end)
