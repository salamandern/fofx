local test = require("lib/gambiarra")
local Util = require("util/util")
local Point = require("model/point")
test("shallowEqualArrays", function()
  ok(Util.shallowEqualArrays({ }, { }), "Empty arrays")
  ok(Util.shallowEqualArrays({
    1
  }, {
    1
  }), "One number")
  ok(Util.shallowEqualArrays({
    "qwe"
  }, {
    "qwe"
  }), "One string")
  ok(Util.shallowEqualArrays({
    math.sin
  }, {
    math.sin
  }), "Empty arrays")
  ok(Util.shallowEqualArrays({
    true
  }, {
    true
  }), "One bool")
  ok(Util.shallowEqualArrays({
    1,
    2
  }, {
    1,
    2
  }), "Two numbers")
  ok(Util.shallowEqualArrays({
    false,
    3,
    "ewq"
  }, {
    false,
    3,
    "ewq"
  }), "Mixed values")
  ok(Util.shallowEqualArrays({
    Point(13, 37)
  }, {
    Point(13, 37)
  }), "With __eq")
  local t = { }
  ok(Util.shallowEqualArrays({
    t
  }, {
    t
  }), "Same table element")
  ok(not Util.shallowEqualArrays({
    4
  }, { }), "Empty right")
  ok(not Util.shallowEqualArrays({ }, {
    6
  }), "Empty left")
  ok(not Util.shallowEqualArrays({
    3
  }, {
    1
  }), "One different number")
  ok(not Util.shallowEqualArrays({
    "abc"
  }, {
    "qwe"
  }), "One different string")
  ok(not Util.shallowEqualArrays({
    math.sin
  }, {
    math.cos
  }), "One different function")
  ok(not Util.shallowEqualArrays({
    2
  }, {
    "qwe"
  }), "Number and string")
  ok(not Util.shallowEqualArrays({
    "qwe"
  }, {
    1
  }), "String and number")
  ok(not Util.shallowEqualArrays({
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    9
  }, {
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8
  }), "Almost equal sequence of numbers")
  ok(not Util.shallowEqualArrays({
    { }
  }, { }), "Different nesting left")
  ok(not Util.shallowEqualArrays({ }, {
    { }
  }), "DIfferent nesting right")
  ok(not Util.shallowEqualArrays({
    {
      1
    }
  }, {
    {
      2
    }
  }), "Differnet 2-nested values")
  ok(not Util.shallowEqualArrays({
    {
      {
        1
      }
    }
  }, {
    {
      {
        2
      }
    }
  }), "Different 3-nested values")
  ok(not Util.shallowEqualArrays({
    { },
    { }
  }, {
    { },
    {
      { }
    }
  }), "Defferent deeper nesting")
  ok(not Util.shallowEqualArrays({
    { }
  }, {
    { }
  }), "Equal One-level Nested empty tables")
  ok(not Util.shallowEqualArrays({
    { },
    {
      { }
    }
  }, {
    { },
    {
      { }
    }
  }), "Equal Deeply nested tables")
  return ok(not Util.shallowEqualArrays({
    1,
    {
      2,
      3
    },
    4
  }, {
    1,
    {
      2,
      3
    },
    4
  }), "Equal Nested tables with values")
end)
test("empty", function()
  ok(Util.empty({ }), "Empty table")
  ok(Util.empty({
    a = "qwe"
  }), "Hash table table")
  ok(not Util.empty({
    1
  }), "One element")
  return ok(not Util.empty({
    1,
    2,
    3,
    4
  }), "Many elements")
end)
local isRgbValue
isRgbValue = function(val)
  return type(val) == "number" and 0 <= val and val <= 255
end
local isColorValid
isColorValid = function(col)
  return (type(col) == "table") and isRgbValue(col[1]) and isRgbValue(col[2]) and isRgbValue(col[3]) and isRgbValue(col[4])
end
return test("randcolor", function()
  eqok(type(Util.randcolor()), "table", "Gives a table")
  ok(isRgbValue(Util.randcolor()[1]), "R is rgb")
  ok(isRgbValue(Util.randcolor()[2]), "G is rgb")
  ok(isRgbValue(Util.randcolor()[3]), "B is rgb")
  ok(isRgbValue(Util.randcolor()[4]), "A is rgb")
  for _ = 0, 1000 do
    local val = Util.randcolor()
    if not isColorValid(val) then
      ok(false, "Statistical: Bad RGBA: " .. tostring(val))
      break
    end
  end
end)
