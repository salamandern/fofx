local test = require("lib/gambiarra")
local moon = require("moon")
local Util = require("util/util")
local Level = require("model/level")
local Function = require("model/function")
local Detector = require("model/detector")
local Point = require("model/point")
local Hint = require("model/hint")
test("Empty constructor", function()
  local level = Level()
  eqok(level.functions, { }, "functions")
  eqok(level.detectors, { }, "detectors")
  eqok(level.functionBank, { }, "functionBank")
  return ok(level.hint == Hint(), "hint")
end)
test("FromSpec", function()
  local spec = {
    functions = { },
    detectors = { },
    functionBank = { }
  }
  local level = Level.fromSpec(spec)
  ok(Util.empty(level.functions), "empty functions")
  ok(Util.empty(level.detectors), "empty detectors")
  ok(Util.empty(level.functionBank), "empty functionBank")
  ok(level.hint == Hint(), "Empty hint default if hint lacking")
  spec = {
    functions = {
      Function.sin:copy()
    },
    detectors = {
      Detector()
    },
    functionBank = {
      Function.cos:copy()
    },
    hint = {
      text = "foo",
      duration = 13
    }
  }
  level = Level.fromSpec(spec)
  ok(Util.shallowEqualArrays(spec.functions, level.functions), "One elem functions")
  ok(Util.shallowEqualArrays(spec.detectors, level.detectors), "One elem detectors")
  ok(Util.shallowEqualArrays(spec.functionBank, level.functionBank), "One elem functionBank")
  ok(level.hint == Hint("foo", 13), "Hint")
  spec = {
    functions = {
      Function.sin:copy(),
      Function.cos:copy()
    },
    detectors = {
      Detector(),
      Detector(Point(3, 2))
    },
    functionBank = {
      Function.cos:copy(),
      Function.sin:copy()
    }
  }
  level = Level.fromSpec(spec)
  ok(Util.shallowEqualArrays(spec.functions, level.functions), "Multiple functions")
  ok(Util.shallowEqualArrays(spec.detectors, level.detectors), "Multiple detectors")
  ok(Util.shallowEqualArrays(spec.functionBank, level.functionBank), "Multiple functionBank")
  spec = {
    detectors = {
      Detector(),
      Detector(Point(3, 2))
    },
    functionBank = {
      Function.cos:copy(),
      Function.sin:copy()
    }
  }
  ok(not (pcall(Level.fromSpec, spec)), "Crashes without functions")
  spec = {
    functions = {
      Function.sin:copy(),
      Function.cos:copy()
    },
    functionBank = {
      Function.cos:copy(),
      Function.sin:copy()
    }
  }
  ok(not (pcall(Level.fromSpec, spec)), "Crashes without detectors")
  spec = {
    functions = {
      Function.sin:copy(),
      Function.cos:copy()
    },
    detectors = {
      Detector(),
      Detector(Point(3, 2))
    }
  }
  ok(not (pcall(Level.fromSpec, spec)), "Crashes without functionBank")
  spec = { }
  return ok(not (pcall(Level.fromSpec, spec)), "Crashes without all")
end)
return test("Equality", function()
  ok(Level() == Level(), "Empty constr")
  local a = Level()
  local b = Level()
  a.functions = {
    Function.sin:copy()
  }
  b.functions = {
    Function.sin:copy()
  }
  ok(a == b, "Functions")
  a = Level()
  b = Level()
  a.detectors = {
    Detector()
  }
  b.detectors = {
    Detector()
  }
  ok(a == b, "Detectors")
  a = Level()
  b = Level()
  a.functionBank = {
    Function.sin:copy()
  }
  b.functionBank = {
    Function.sin:copy()
  }
  ok(a == b, "function bank")
  a = Level()
  b = Level()
  a.functions = {
    Function.sin:copy()
  }
  b.functions = {
    Function.sin:copy()
  }
  a.detectors = {
    Detector()
  }
  b.detectors = {
    Detector()
  }
  a.functionBank = {
    Function.sin:copy()
  }
  b.functionBank = {
    Function.sin:copy()
  }
  ok(a == b, "All")
  a = Level()
  b = Level()
  a.functions = {
    Function.sin:copy()
  }
  ok(a ~= b, "Functions different")
  a = Level()
  b = Level()
  a.detectors = {
    Detector()
  }
  ok(a ~= b, "Detectors different")
  a = Level()
  b = Level()
  a.functionBank = {
    Function.sin:copy()
  }
  ok(a ~= b, "function bank")
  a = Level()
  b = Level()
  a.functions = {
    Function.sin:copy()
  }
  b.functions = {
    Function.cos:copy()
  }
  a.detectors = {
    Detector()
  }
  b.detectors = {
    Detector()
  }
  a.functionBank = {
    Function.sin:copy()
  }
  b.functionBank = {
    Function.sin:copy()
  }
  ok(a ~= b, "Different functions")
  a = Level()
  b = Level()
  a.functions = {
    Function.sin:copy()
  }
  b.functions = {
    Function.sin:copy()
  }
  a.detectors = {
    Detector()
  }
  b.detectors = {
    Detector(Point(3, 2))
  }
  a.functionBank = {
    Function.sin:copy()
  }
  b.functionBank = {
    Function.sin:copy()
  }
  ok(a ~= b, "Different detector")
  a = Level()
  b = Level()
  a.functions = {
    Function.sin:copy()
  }
  b.functions = {
    Function.sin:copy()
  }
  a.detectors = {
    Detector()
  }
  b.detectors = {
    Detector()
  }
  a.functionBank = {
    Function.sin:copy()
  }
  b.functionBank = {
    Function.cos:copy()
  }
  return ok(a ~= b, "Different functionbank function")
end)
