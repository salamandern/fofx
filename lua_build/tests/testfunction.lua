local test = require("lib/gambiarra")
local Function = require("model/function")
local Op = require("model/operator")
test("Basic functions work", function()
  eqok(Function.id(0), 0, "id 0")
  eqok(Function.id(4), 4, "id 4")
  eqok(Function.id(-1), -1, "id -1")
  eqok(Function.sin(0), 0, "sin 0")
  eqok(Function.sin(math.pi / 2), 1, "sin 0.5rad")
  eqok(Function.sin(math.pi), math.sin(math.pi), "sin 1rad")
  eqok(Function.cos(0), 1, "cos 0")
  eqok(Function.cos(math.pi / 2), math.cos(math.pi / 2), "cos 0.5rad")
  return eqok(Function.cos(math.pi), -1, "cos 1rad")
end)
test("Copies of basic functions work", function()
  eqok(Function.id:copy()(0), 0, "id 0")
  eqok(Function.id:copy()(4), 4, "id 4")
  eqok(Function.id:copy()(-1), -1, "id -1")
  eqok(Function.sin:copy()(0), math.sin(0), "sin 0")
  eqok(Function.sin:copy()(math.pi / 2), math.sin(math.pi / 2), "sin 0.5rad")
  eqok(Function.sin:copy()(math.pi), math.sin(math.pi), "sin 1rad")
  eqok(Function.cos:copy()(0), math.cos(0), "cos 0")
  eqok(Function.cos:copy()(math.pi / 2), math.cos(math.pi / 2), "cos 0.5rad")
  return eqok(Function.cos:copy()(math.pi), math.cos(math.pi), "cos 1rad")
end)
test("Constructor options", function()
  local f
  f = function() end
  local name = "foo"
  local col = {
    255,
    0,
    0
  }
  local fObj = Function(f, name, col)
  eqok(fObj.f, f, "f")
  eqok(fObj.name, name, "name")
  return eqok(fObj.color, col, "col")
end)
test("Copy options", function()
  local fObj = Function(function() end, "zinArc", {
    123,
    43,
    43
  })
  local f
  f = function() end
  local name = "foo"
  local col = {
    255,
    0,
    0
  }
  local fObj2 = fObj:copy({
    innerF = f,
    name = name,
    color = col,
    xStretch = 17
  })
  eqok(fObj2.innerF, f, "f")
  eqok(fObj2.name, name, "name")
  eqok(fObj2.color, col, "col")
  return eqok(fObj2.xStretch, 17, "xStretch")
end)
test("Combining", function()
  local f = Function.sin:copy()
  local g = Function.cos:copy()
  local fg = Function:combine(Op.Add, f, g)
  eqok(fg(1), math.sin(1) + math.cos(1), "Math functions added")
  fg.yStretch = 2
  eqok(fg(1), 2 * (math.sin(1) + math.cos(1)), "Func still usable")
  f.yStretch = 2
  return eqok(fg(1), 4 * math.sin(1) + 2 * math.cos(1), "subf changes propagate")
end)
return test("Equality", function()
  ok(Function() == Function.id:copy(), "empty constructor equals id")
  local f = Function.sin:copy()
  local g = Function.cos:copy()
  ok(Function() == Function(), "Empty constructor")
  ok(f ~= g, "Different functions")
  ok(f == f:copy(), "Copy")
  local fg1 = Function:combine(Op.Add, f, g)
  local fg2 = Function:combine(Op.Add, f, g)
  local fgd1 = Function:combine(Op.Add, f, f)
  local fgd2 = Function:combine(Op.Add, g, g)
  local sw = Function:combine(Op.Add, g, f)
  ok(fg1 == fg2, "Combination")
  ok(fg1 ~= fgd1, "right differs")
  ok(fg1 ~= fgd2, "left differs")
  ok(fgd1 ~= fgd2, "Both differ")
  ok(fg1 ~= sw, "Switched")
  local fplusg = Function:combine(Op.Add, f, g)
  local fsubg = Function:combine(Op.Subtract, f, g)
  ok(fplusg ~= fsubg, "Different op")
  local deep1 = Function:combine(Op.Add, Function:combine(Op.Add, f, g), Function:combine(Op.Add, f, g))
  local deep2 = Function:combine(Op.Add, Function:combine(Op.Add, f, g), Function:combine(Op.Add, f, g))
  local deep_other1 = Function:combine(Op.Add, Function:combine(Op.Add, g, g), Function:combine(Op.Add, f, g))
  local deep_other2 = Function:combine(Op.Add, Function:combine(Op.Add, f, g), Function:combine(Op.Divide, f, g))
  ok(deep1 == deep2, "Deeply nested equal")
  ok(deep1 ~= deep_other1, "Deeply nested differ on function")
  ok(deep1 ~= deep_other2, "Deeply nested differ on Op")
  f = Function.sin:copy()
  g = Function.sin:copy()
  g.xOffset = 17
  ok(f ~= g, "Diff on xOffset")
  f = Function.sin:copy()
  g = Function.sin:copy()
  g.yOffset = 17
  ok(f ~= g, "Diff on yOffset")
  f = Function.sin:copy()
  g = Function.sin:copy()
  g.xStretch = 17
  ok(f ~= g, "Diff on xStretch")
  f = Function.sin:copy()
  g = Function.sin:copy()
  g.yStretch = 17
  ok(f ~= g, "Diff on yStretch")
  f = Function.sin:copy()
  g = Function.sin:copy()
  g.name = "qweqweqweqwe"
  ok(f ~= g, "Diff on name")
  f = Function.sin:copy()
  g = Function.sin:copy()
  g.col = "qweqweqweqwe"
  ok(f == g, "No diff on color")
  f = Function:combine(Op.Add, Function.sin:copy(), Function:combine(Op.Multiply, Function.cos:copy(), Function.tan:copy()))
  g = Function:combine(Op.Add, Function.sin:copy(), Function:combine(Op.Multiply, Function.cos:copy(), Function.tan:copy()))
  return ok(f == g, "Identical constructor example")
end)
