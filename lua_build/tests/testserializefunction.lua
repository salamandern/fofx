local test = require("lib/gambiarra")
local Function = require("model/function")
local Operator = require("model/operator")
local ser = require("util/serializefunction")
test("Function names", function()
  eqok(ser(Function.id), "id", "id")
  eqok(ser(Function.sin), "sin", "sin")
  eqok(ser(Function.cos), "cos", "cos")
  eqok(ser(Function.tan), "tan", "tan")
  eqok(ser(Function.asin), "asin", "asin")
  eqok(ser(Function.acos), "acos", "acos")
  eqok(ser(Function.atan), "atan", "atan")
  eqok(ser(Function.abs), "abs", "abs")
  eqok(ser(Function.ceil), "ceil", "ceil")
  eqok(ser(Function.floor), "floor", "floor")
  eqok(ser(Function.log), "log", "log")
  eqok(ser(Function.log10), "log10", "log10")
  eqok(ser(Function.sqrt), "sqrt", "sqrt")
  return nil
end)
test("Operator names", function()
  eqok(ser(Function:combine(Operator.Add, Function.sin, Function.cos)), "+ sin cos", "Add")
  eqok(ser(Function:combine(Operator.Subtract, Function.sin, Function.cos)), "- sin cos", "Subtract")
  eqok(ser(Function:combine(Operator.Multiply, Function.sin, Function.cos)), "* sin cos", "Multiply")
  eqok(ser(Function:combine(Operator.Divide, Function.sin, Function.cos)), "/ sin cos", "Divide")
  return eqok(ser(Function:combine(Operator.Compose, Function.sin, Function.cos)), "o sin cos", "Compose")
end)
test("Nested ops", function()
  local f = Function.sin:copy()
  local g = Function.cos:copy()
  local h = Function.tan:copy()
  local i = Function.log:copy()
  local fg = Function:combine(Operator.Subtract, f, g)
  local fgh = Function:combine(Operator.Add, fg, h)
  eqok(ser(fgh), "+ - sin cos tan", "Left branch")
  local gh = Function:combine(Operator.Subtract, g, h)
  fgh = Function:combine(Operator.Add, f, gh)
  eqok(ser(fgh), "+ sin - cos tan", "Right branch")
  fg = Function:combine(Operator.Add, f, g)
  local hi = Function:combine(Operator.Subtract, h, i)
  local fghi = Function:combine(Operator.Multiply, fg, hi)
  eqok(ser(fghi), "* + sin cos - tan log", "Both branches")
  local ff = Function:combine(Operator.Add, f, f:copy())
  local fff = Function:combine(Operator.Add, ff, f:copy())
  local ffff = Function:combine(Operator.Add, fff, f:copy())
  local fffff = Function:combine(Operator.Add, ffff, f:copy())
  eqok(ser(fffff), "+ + + + sin sin sin sin sin", "Deep a f")
  f = Function:combine(Operator.Add, Function.sin:copy(), Function:combine(Operator.Multiply, Function.cos:copy(), Function.tan:copy()))
  return eqok(ser(f), "+ sin * cos tan", "An example")
end)
test("Function modifiers", function()
  local id = Function.id:copy()
  id.yStretch = 11
  eqok(ser(id), "(11,)id", "yStretch")
  id = Function.id:copy()
  id.yOffset = 11
  eqok(ser(id), "(,11)id", "yOffset")
  id = Function.id:copy()
  id.xStretch = 11
  eqok(ser(id), "id(11,)", "xStretch")
  id = Function.id:copy()
  id.xOffset = 11
  eqok(ser(id), "id(,11)", "xOffset")
  id = Function.id:copy()
  id.yStretch = 11
  id.yOffset = 12
  id.xStretch = 13
  id.xOffset = 14
  return eqok(ser(id), "(11,12)id(13,14)", "All")
end)
return test("Op modifiers", function()
  local f = Function.sin:copy()
  f.yStretch = 15
  f.yOffset = 16
  f.xStretch = 17
  f.xOffset = 18
  local g = Function.cos:copy()
  g.yStretch = 19
  g.yOffset = 20
  g.xStretch = 21
  g.xOffset = 22
  local fg = Function:combine(Operator.Multiply, f, g)
  fg.yStretch = 11
  fg.yOffset = 12
  fg.xStretch = 13
  fg.xOffset = 14
  local expect = "(11,12)*(13,14) (15,16)sin(17,18) (19,20)cos(21,22)"
  eqok(ser(fg), expect, "Operator with modifiers")
  f = Function.sin:copy()
  g = Function.cos:copy()
  g.yStretch = 15
  local h = Function.tan:copy()
  fg = Function:combine(Operator.Subtract, f, g)
  fg.yStretch = 11
  fg.yOffset = 12
  fg.xStretch = 13
  fg.xOffset = 14
  local fgh = Function:combine(Operator.Add, fg, h)
  return eqok(ser(fgh), "+ (11,12)-(13,14) sin (15,)cos tan", "Nested modifiers")
end)
