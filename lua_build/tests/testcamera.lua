love = {
  graphics = { }
}
local test = require("lib/gambiarra")
local Point = require("model/point")
local Camera = require("model/camera")
local assumeHH = 2
local assumeHW = 2.8
local assumeDims = Point(assumeHW, assumeHH) * 2
local windowDims = Point(800, 600)
local screenMid = windowDims / 2
test("Construction", function()
  local cam = Camera(3, 4)
  ok(cam.hdim.y == assumeHH and cam.hdim.x == assumeHW, "No flexibility for yuo!")
  return ok(cam.pos.x == 3 and cam.pos.y == 4, "Point constructor")
end)
return test("Scaling", function()
  local dimSpy = spy(function()
    return windowDims.x, windowDims.y
  end)
  local widSpy = spy(function()
    return windowDims.x
  end)
  local heiSpy = spy(function()
    return windowDims.y
  end)
  love.graphics.getDimensions = dimSpy
  love.graphics.getWidth = widSpy
  love.graphics.getHeight = heiSpy
  local cam = Camera()
  eqok(cam:coordinateScale(), Point(1, -1) * windowDims / assumeDims, "Correct scale")
  ok(dimSpy.called or widSpy.called and heiSpy.called, "Asks löve for info")
  eqok(cam:worldToScreen(Point()), screenMid, "Origin in center")
  local unitCircle = {
    Point(1, 0),
    Point(0, 1),
    Point(-1, 0),
    Point(0, -1)
  }
  local screenUnitCircle = {
    screenMid + Point(screenMid.x / assumeHW, 0),
    screenMid + Point(0, -screenMid.y / assumeHH),
    screenMid + Point(-screenMid.x / assumeHW, 0),
    screenMid + Point(0, screenMid.y / assumeHH)
  }
  for i, p in ipairs(unitCircle) do
    eqok(tostring(cam:worldToScreen(p)), tostring(screenUnitCircle[i]), "UnitC world to screen")
  end
  for i, p in ipairs(screenUnitCircle) do
    eqok(tostring(cam:screenToWorld(p)), tostring(unitCircle[i]), "UnitC screenToWorld")
  end
end)
