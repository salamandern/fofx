local test = require("lib/gambiarra")
local Model = require("model/model")
return test("Worlds separate", function()
  local w1 = Model()
  local w2 = Model()
  w1.time = 1337
  ok(w1.time ~= w2.time, "Changing var does not change var")
  w1.camera.x = 13
  return ok(w1.camera.x ~= w2.camera.x, "Camera")
end)
