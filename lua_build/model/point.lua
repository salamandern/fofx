local Util = require("util/util")
local Point
do
  local _class_0
  local _base_0 = {
    __eq = function(self, other)
      return self.x == other.x and self.y == other.y
    end,
    __unm = function(self)
      return Point(-self.x, -self.y)
    end,
    __tostring = function(self)
      return "(" .. self.x .. "," .. self.y .. ")"
    end,
    __inherited = function(self, cls)
      cls.__base.__add = self.__add
      cls.__base.__sub = self.__sub
      cls.__base.__mul = self.__mul
      cls.__base.__div = self.__div
      cls.__base.__unm = self.__unm
      cls.__base.__tostring = self.__tostring
    end,
    coords = function(self)
      return self.x, self.y
    end,
    inside = function(self, a, b)
      return Util.between(a.x, self.x, b.x) and Util.between(a.x, self.x, b.x)
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, x, y)
      if x == nil then
        x = 0
      end
      if y == nil then
        y = 0
      end
      self.x, self.y = x, y
    end,
    __base = _base_0,
    __name = "Point"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Point = _class_0
end
local opfunct
opfunct = function(op)
  return function(a, b)
    if type(a) == "table" then
      if type(b) == "table" then
        return Point(op(a.x, b.x), op(a.y, b.y))
      else
        return Point(op(a.x, b), op(a.y, b))
      end
    else
      return Point(op(a, b.x), op(a, b.y))
    end
  end
end
Point.__base.__add = opfunct(function(a, b)
  return a + b
end)
Point.__base.__sub = opfunct(function(a, b)
  return a - b
end)
Point.__base.__mul = opfunct(function(a, b)
  return a * b
end)
Point.__base.__div = opfunct(function(a, b)
  return a / b
end)
return Point
