local Hint
do
  local _class_0
  local _base_0 = {
    __eq = function(self, other)
      return (self.text == other.text) and (self.duration == other.duration)
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, text, duration)
      if text == nil then
        text = ""
      end
      if duration == nil then
        duration = 0
      end
      self.text, self.duration = text, duration
      self.timeLeft = self.duration
    end,
    __base = _base_0,
    __name = "Hint"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Hint = _class_0
end
return Hint
