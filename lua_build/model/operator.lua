local Operator
do
  local _class_0
  local _base_0 = {
    paddedName = function(self)
      return " " .. self.name .. " "
    end,
    serialize = function(self, parts)
      if self.nOperands == 2 then
        assert(#parts == 2)
        return ("(" .. parts[1]:serialize() .. ")" .. self:paddedName() .. "(" .. parts[2]:serialize() .. ")")
      end
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, name, evaluate, nOperands)
      self.name, self.evaluate, self.nOperands = name, evaluate, nOperands
      self._dontcopy = true
    end,
    __base = _base_0,
    __name = "Operator"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Operator = _class_0
end
Operator.__base.Add = Operator("+", (function(x, a, b)
  return a(x) + b(x)
end), 2)
Operator.__base.Subtract = Operator("-", (function(x, a, b)
  return a(x) - b(x)
end), 2)
Operator.__base.Multiply = Operator("*", (function(x, a, b)
  return a(x) * b(x)
end), 2)
Operator.__base.Divide = Operator("/", (function(x, a, b)
  return a(x) / b(x)
end), 2)
Operator.__base.Compose = Operator("o", (function(x, a, b)
  return a(b(x))
end), 2)
return Operator
