local Util = require("util/util")
local Camera = require("model/camera")
local Model
do
  local _class_0
  local _base_0 = {
    isValidFunctionId = function(self, id)
      return id > 0 and id <= #self.functions
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, level)
      if level == nil then
        level = { }
      end
      self.time = 0
      self.functions = { }
      self.currentFunction = 1
      self.camera = Camera()
      self.detectors = { }
      self.adapt = "xy"
      self.isAdapting = false
      self.levelMetadata = nil
      self.winTimer = nil
      self.shouldChangeToNext = false
      self.locked = false
      self.register1 = -1
      self.register2 = -1
      return Util.overrideEntries(self, level)
    end,
    __base = _base_0,
    __name = "Model"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Model = _class_0
end
return Model
