local Util = require("util/util")
local Operator = require("model/operator")
local Function
do
  local _class_0
  local _base_0 = {
    __call = function(self, x)
      x = x * self.xStretch - self.xOffset
      local fofx = nil
      if self.operator then
        assert(self.operator.nOperands == #self.parts)
        local partYs = Util.map(Util.evalWith(x), self.parts)
        fofx = self.operator.evaluate(x, unpack(self.parts))
      else
        fofx = self.f(x)
      end
      local y = fofx * self.yStretch + self.yOffset
      return y
    end,
    __eq = function(self, other)
      return (self.name == other.name) and (self.f == other.f) and (self.xOffset == other.xOffset) and (self.yOffset == other.yOffset) and (self.xStretch == other.xStretch) and (self.yStretch == other.yStretch) and (self.operator == other.operator) and Util.shallowEqualArrays(self.parts, other.parts)
    end,
    copy = function(self, o)
      if o == nil then
        o = { }
      end
      local result = Util.shallowCopyKeepMt(self)
      Util.overrideEntries(result, o)
      return result
    end,
    combine = function(self, op, f, g)
      local result = self.__class.id:copy()
      result.operator = op
      assert((op.nOperands == 1) == (g == nil), "Function\\combine called with bad function amount")
      result.parts = g and {
        f,
        g
      } or {
        f
      }
      return result
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, f, name, color)
      if f == nil then
        f = Function.f_id
      end
      if name == nil then
        name = "id"
      end
      if color == nil then
        color = {
          255,
          255,
          255
        }
      end
      self.f, self.name, self.color = f, name, color
      self.xOffset = 0
      self.yOffset = 0
      self.xStretch = 1
      self.yStretch = 1
      self.parts = { }
      self.operator = nil
    end,
    __base = _base_0,
    __name = "Function"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  local self = _class_0
  self.f_id = function(x)
    return x
  end
  Function = _class_0
end
Function.__base.id = Function(Function.f_id, "id")
Function.__base.sin = Function(math.sin, "sin")
Function.__base.cos = Function(math.cos, "cos")
Function.__base.tan = Function(math.tan, "tan")
Function.__base.asin = Function(math.asin, "asin")
Function.__base.acos = Function(math.acos, "acos")
Function.__base.atan = Function(math.atan, "atan")
Function.__base.abs = Function(math.abs, "abs")
Function.__base.ceil = Function(math.ceil, "ceil")
Function.__base.floor = Function(math.floor, "floor")
Function.__base.log = Function(math.log, "log")
Function.__base.log10 = Function(math.log10, "log10")
Function.__base.sqrt = Function(math.sqrt, "sqrt")
Function.__base.cool = Function:combine(Operator.Add, Function.sin:copy(), Function.id:copy())
return Function
