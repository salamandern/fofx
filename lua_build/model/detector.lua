local Point = require("model/point")
local Detector
do
  local _class_0
  local _parent_0 = Point
  local _base_0 = {
    update = function(self, world)
      self.overlapped = false
      for _, f in ipairs(world.functions) do
        if math.abs(self.pos.y - f(self.pos.x)) <= self.span then
          self.overlapped = true
          break
        end
      end
    end,
    isSatisfied = function(self)
      return self.overlapped == self.shouldOverlap
    end,
    __eq = function(self, other)
      return self.pos == other.pos and self.shouldOverlap == other.shouldOverlap and self.span == other.span
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  _class_0 = setmetatable({
    __init = function(self, pos, shouldOverlap, span)
      if pos == nil then
        pos = Point(0, 0)
      end
      if shouldOverlap == nil then
        shouldOverlap = true
      end
      if span == nil then
        span = 0.1
      end
      self.pos, self.shouldOverlap, self.span = pos, shouldOverlap, span
      self.overlapped = false
    end,
    __base = _base_0,
    __name = "Detector",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        local parent = rawget(cls, "__parent")
        if parent then
          return parent[name]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  Detector = _class_0
end
return Detector
