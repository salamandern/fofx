local Point = require("model/point")
local Camera
do
  local _class_0
  local _base_0 = {
    worldToScreen = function(self, p)
      local scale = self:coordinateScale()
      local postTranslate = Point(love.graphics.getDimensions()) / 2
      return (p - self.pos) * scale + postTranslate
    end,
    screenToWorld = function(self, p)
      local scale = self:coordinateScale()
      local postTranslate = Point(love.graphics.getDimensions()) / 2
      return (p - postTranslate) / scale + self.pos
    end,
    coordinateScale = function(self)
      local xScale = love.graphics.getWidth() / (2 * self.hdim.x)
      local yScale = -love.graphics.getHeight() / (2 * self.hdim.y)
      return Point(xScale, yScale)
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, x, y)
      self.pos = Point(x, y)
      self.hdim = Point(2.8, 2)
    end,
    __base = _base_0,
    __name = "Camera"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Camera = _class_0
end
return Camera
