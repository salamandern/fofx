local Util = require("util/util")
local Hint = require("model/hint")
local allLevels
local Level
do
  local _class_0
  local _base_0 = {
    fromSpec = function(spec)
      local ret = Level()
      assert(spec)
      assert(spec.functions)
      assert(spec.detectors)
      assert(spec.functionBank)
      ret.functions = Util.deepCopyKeepMt(spec.functions)
      ret.detectors = Util.deepCopyKeepMt(spec.detectors)
      ret.functionBank = Util.deepCopyKeepMt(spec.functionBank)
      if spec.hint then
        ret.hint = Hint(spec.hint.text, spec.hint.duration)
      end
      if not (spec.stretchEnabled == nil) then
        ret.stretchEnabled = spec.stretchEnabled
      end
      return ret
    end,
    __eq = function(self, other)
      return Util.shallowEqualArrays(self.functions, other.functions) and Util.shallowEqualArrays(self.detectors, other.detectors) and Util.shallowEqualArrays(self.functionBank, other.functionBank)
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self)
      self.functions = { }
      self.detectors = { }
      self.functionBank = { }
      self.hint = Hint()
      self.stretchEnabled = true
    end,
    __base = _base_0,
    __name = "Level"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Level = _class_0
end
return Level
