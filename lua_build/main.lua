package.cpath = "./?.dll;./libs/?.dll;"
require("lib/strict")
local moon = require("moon")
local suit = require("lib/SUIT")
local Model = require("model/model")
local View = require("view/view")
local Controller = require("controller/controller")
local Level = require("model/level")
local loadlevel = require("controller/loadlevel")
local kb = love.keyboard
local lg = love.graphics
local lf = love.filesystem
local world = nil
local controller = nil
local changeToNextLevel
local newLevel
local initContainingFolder
love.load = function()
  initContainingFolder()
  newLevel(1)
  print("Saving all files to " .. tostring(love.filesystem.getSaveDirectory()))
  return print("Path for looking for libs: " .. tostring(package.cpath))
end
love.update = function(dt)
  world.time = world.time + dt
  controller:update(dt)
  if world.shouldChangeToNext then
    return changeToNextLevel()
  end
end
love.draw = function()
  View.render(world)
  lg.setColor(255, 255, 255)
  return suit.draw()
end
love.textinput = function(t)
  return suit.textinput(t)
end
love.keypressed = function(key, scancode, isRepeat)
  if scancode == "escape" then
    love.event.quit()
  end
  controller:keypressed(key, scancode, isRepeat)
  return suit.keypressed(key)
end
love.mousemoved = function(x, y, dx, dy)
  return controller:mousemoved(x, y, dx, dy)
end
love.mousepressed = function()
  return controller:mousepressed()
end
love.mousereleased = function()
  return controller:mousereleased()
end
changeToNextLevel = function()
  local nextLevelId = (world.levelMetadata.id % loadlevel.availableLevels()) + 1
  return newLevel(nextLevelId)
end
newLevel = function(id)
  world = Model(loadlevel.serve(id))
  controller = Controller(world)
end
initContainingFolder = function()
  print("Gonna see if I can init the containing folder")
  if not lf.isFused() then
    print("Cannot init containing folder, not running in fused mode!")
    return 
  end
  local containingFolderPath = lf.getSourceBaseDirectory()
  if not containingFolderPath then
    print("Could not get containing forlder path")
    return 
  end
  lf.mount(containingFolderPath, "containingFolder")
  print("Initting containint folder")
end
