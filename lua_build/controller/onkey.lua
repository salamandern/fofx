local Util = require("util/util")
local OnKey
do
  local _class_0
  local _base_0 = {
    add = function(self, actions)
      return Util.overrideEntries(self.actions, actions)
    end,
    run = function(self, id)
      local action = self.actions[id]
      if action then
        if self.subject then
          return action(self.subject, id)
        else
          return action(id)
        end
      end
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, subject)
      self.actions = { }
      self.subject = subject
    end,
    __base = _base_0,
    __name = "OnKey"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  OnKey = _class_0
end
return OnKey
