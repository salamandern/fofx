local moon = require("moon")
local Util = require("util/util")
local OnKeyLib = require("controller/onkey")
local Level = require("model/level")
local Function = require("model/function")
local Operator = require("model/operator")
local Point = require("model/point")
local GUILib = require("view/gui")
local runAfter = require("controller/runafter")
local serializeLevel = (require("util/serializelevel")).serialize
local mouse = love.mouse
local Controller
do
  local _class_0
  local _base_0 = {
    winTime = 2.5,
    update = function(self, dt)
      self.GUI:update()
      self:updateMouseState()
      self:updateDetectors()
      self:checkWinState()
      self:updateWinClock(dt)
      return self:updateHintClock()
    end,
    initWorld = function(self)
      if self.world.hint then
        self.world.hintTimer = runAfter(self.world.hint.duration, function()
          return self:removeHint()
        end)
      end
    end,
    removeHint = function(self)
      self.world.hint = nil
      self.world.hintTimer = nil
    end,
    updateWinClock = function(self, dt)
      if self.world.winTimer then
        return self.world.winTimer()
      end
    end,
    updateHintClock = function(self)
      if self.world.hintTimer then
        return self.world.hintTimer()
      end
    end,
    updateMouseState = function(self)
      return self:updateMouseRelativeMode()
    end,
    updateMouseRelativeMode = function(self)
      local mouseRel = mouse.getRelativeMode()
      if self.world.isAdapting then
        if not mouseRel then
          mouse.setRelativeMode(true)
          local x, y = mouse.getPosition()
          self.savedMousePos = {
            x,
            y
          }
        end
      else
        if mouseRel then
          mouse.setRelativeMode(false)
          if self.savedMousePos then
            return mouse.setPosition(unpack(self.savedMousePos))
          end
        end
      end
    end,
    updateDetectors = function(self)
      for _, d in ipairs(self.world.detectors) do
        d:update(self.world)
      end
    end,
    addNewFunction = function(self, f)
      f.color = Util.randcolor()
      table.insert(self.world.functions, f)
      return self:resetFunctionMetadata()
    end,
    addRandomFunction = function(self)
      local newF = Function.sin:copy({
        yOffset = math.random()
      })
      return self:addNewFunction(newF)
    end,
    addCombinationFunction = function(self, op)
      assert((moon.type(op) == Operator), "op should be and operator")
      if self.world:isValidFunctionId(self.world.register1) and self.world:isValidFunctionId(self.world.register2) then
        local a1 = self.world.functions[self.world.register1]
        local a2 = self.world.functions[self.world.register2]
        local newF = Function:combine(op, a1, a2)
        return self:addNewFunction(newF)
      end
    end,
    removeLastFunction = function(self)
      table.remove(self.world.functions)
      return self:resetFunctionMetadata()
    end,
    setCurrentFunction = function(self, id)
      if 1 <= id and id <= #self.world.functions then
        self.world.currentFunction = id
      end
    end,
    checkWinState = function(self)
      local allCorrect = true
      for _, d in ipairs(self.world.detectors) do
        if not d:isSatisfied() then
          allCorrect = false
          break
        end
      end
      if allCorrect and not self.world.winTimer and not self.world.isAdapting then
        return self:setWinTimer()
      end
    end,
    setWinTimer = function(self)
      self.world.winTimer = runAfter(self.winTime, function()
        return self:winLevel()
      end)
      self.world.locked = true
      self.world.isAdapting = false
    end,
    winLevel = function(self)
      print("WOHOOW")
      self.world.locked = false
      self.world.winTimer = nil
      self.world.shouldChangeToNext = true
    end,
    resetFunctionMetadata = function(self)
      local nFunctions = #self.world.functions
      if self.world.currentFunction > nFunctions then
        self.world.currentFunction = nFunctions
      end
      if self.world.currentFunction < 1 then
        self.world.currentFunction = 1
      end
      self.world.register1 = -1
      self.world.register2 = -1
    end,
    switchAdaptationStyle = function(self)
      if self.world.adapt == "xy" and self.world.stretchEnabled then
        self.world.adapt = "stretch"
      else
        self.world.adapt = "xy"
      end
    end,
    dumpLevelToFile = function(self, name)
      local data = "-----------\n" .. self:dumpLevel()
      local filename = tostring(name) .. ".fworld"
      local success, err = love.filesystem.append(filename, data)
      if not success then
        return print("WARNING: could not dump world to " .. tostring(filename) .. ". Error: " .. tostring(err))
      end
    end,
    dumpLevel = function(self)
      return serializeLevel(self.world)
    end,
    keypressed = function(self, key, scancode, isRepeat)
      return self.OnKey:run(scancode)
    end,
    mousemoved = function(self, x, y, dx, dy)
      if self.world.isAdapting then
        local worldDelta = Point(dx, dy) / self.world.camera:coordinateScale()
        local f = self.world.functions[self.world.currentFunction]
        if f then
          if self.world.adapt == "xy" then
            f.yOffset = f.yOffset + worldDelta.y
            f.xOffset = f.xOffset + worldDelta.x
          else
            f.xStretch = f.xStretch + worldDelta.x
            f.yStretch = f.yStretch + worldDelta.y
          end
        end
      end
    end,
    mousepressed = function(self)
      if not (self.GUI.hasMouse or self.world.locked) then
        self.world.isAdapting = true
      end
    end,
    mousereleased = function(self)
      self.world.isAdapting = false
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, newWorld)
      self.world = newWorld
      self.GUI = GUILib(self.world, self)
      self.OnKey = OnKeyLib(self)
      self.savedMousePos = nil
      self:resetFunctionMetadata(self)
      local onNumber
      onNumber = function(self, n)
        return self:setCurrentFunction(tonumber(n))
      end
      self.OnKey:add({
        ["1"] = onNumber,
        ["2"] = onNumber,
        ["3"] = onNumber,
        ["4"] = onNumber,
        ["5"] = onNumber,
        ["6"] = onNumber,
        ["7"] = onNumber,
        ["8"] = onNumber,
        ["9"] = onNumber,
        x = self.switchAdaptationStyle,
        l = self.winLevel,
        t = self.setWinTimer,
        a = self.addRandomFunction,
        r = self.removeLastFunction
      })
      return self:initWorld()
    end,
    __base = _base_0,
    __name = "Controller"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Controller = _class_0
end
return Controller
