return function(runAfterSecs, callback)
  local startTime = os.time()
  local updateClock
  updateClock = function()
    local timediff = os.difftime(os.time(), startTime)
    local done = timediff >= runAfterSecs
    if done and callback then
      print("Calling back!")
      callback()
      callback = nil
    end
    return done
  end
  return updateClock
end
