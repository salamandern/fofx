local pl = (require("pl.import_into"))()
local moon = require("moon")
local Util = require("util/util")
local Level = require("model/level")
local deserl = (require("util/serializelevel")).deserialize
local lf = love.filesystem
local LEVELS_DIR = "containingFolder/levels"
local availableLevels, sortedAvailableLevelNames, validLevelName, serve
availableLevels = function()
  return #sortedAvailableLevelNames()
end
sortedAvailableLevelNames = function()
  local containingFolderItems = lf.getDirectoryItems(LEVELS_DIR)
  local lvlNames = pl.tablex.filter(containingFolderItems, validLevelName)
  table.sort(lvlNames)
  return lvlNames
end
validLevelName = function(levelName)
  return (not (lf.isDirectory(levelName))) and Util.isYamlFile(levelName)
end
serve = function(id)
  local lvlNames = sortedAvailableLevelNames()
  if id <= 0 or #lvlNames < id then
    return nil
  else
    local lvlName = lvlNames[id]
    local strLevel = lf.read(tostring(LEVELS_DIR) .. "/" .. tostring(lvlName))
    local ret = deserl(strLevel)
    ret.levelMetadata = {
      id = id
    }
    return ret
  end
end
return {
  availableLevels = availableLevels,
  serve = serve
}
