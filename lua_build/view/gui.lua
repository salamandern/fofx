local suit = require("lib/SUIT")
local Artist = require("view/artist")
local Operator = require("model/operator")
local GUI
do
  local _class_0
  local _base_0 = {
    update = function(self, dt)
      self.hasMouse = false
      if not self.world.isAdapting then
        self:functionButtons()
        self:adaptButtons()
        self:functionBankGUI()
        self:registerButtons()
        self:combinationButtons()
        self:devButtons()
      end
      self.hasMouse = suit:anyHovered()
    end,
    fDependsOn = function(self, f, g)
      for _, p in ipairs(f.parts) do
        if p == g or GUI:fDependsOn(p, g) then
          return true
        end
      end
      return false
    end,
    functionButtons = function(self)
      local noteCurrentFunction = 1 < #self.world.functions
      local cf = self.world.functions[self.world.currentFunction]
      for i, f in ipairs(self.world.functions) do
        local x = 60 * i - 40
        local y = 540
        local img = Artist.fButtonImage(f.color or Util.col.white)
        if noteCurrentFunction then
          if i == self.world.currentFunction then
            img = Artist.fSelectedButtonImage(f.color or Util.col.white)
          elseif GUI:fDependsOn(f, cf) or GUI:fDependsOn(cf, f) then
            img = Artist.fDepButtonImage(f.color or Util.col.white)
          end
        end
        if suit.ImageButton(img, x, y).hit then
          print("Setting function to" .. i)
          self.controller:setCurrentFunction(i)
        end
      end
    end,
    adaptButtons = function(self)
      local world = self.world
      if world.stretchEnabled then
        local text = world.adapt == "stretch" and "STRETCH" or "stretch"
        if suit.Button(text, 700, 550, 40, 40).hit then
          world.adapt = "stretch"
        end
      end
      local text = world.adapt == "xy" and "MOVE" or "move"
      if suit.Button(text, 750, 550, 40, 40).hit then
        world.adapt = "xy"
      end
    end,
    functionBankGUI = function(self)
      for i, f in ipairs(self.world.functionBank) do
        if suit.Button(f.name, 20, -30 + 50 * i, 100, 30).hit then
          self.controller:addNewFunction(f:copy())
        end
      end
    end,
    registerButtons = function(self)
      local world = self.world
      local label1 = ""
      local label2 = ""
      if world:isValidFunctionId(world.register1) then
        label1 = tostring(world.register1)
      end
      if world:isValidFunctionId(world.register2) then
        label2 = tostring(world.register2)
      end
      if suit.Button("1: " .. label1, 700, 10, 40, 40).hit then
        world.register1 = world.currentFunction
      end
      if suit.Button("2: " .. label2, 750, 10, 40, 40).hit then
        world.register2 = world.currentFunction
      end
    end,
    combinationButtons = function(self)
      if suit.Button("+", 750, 60, 40, 40).hit then
        self.controller:addCombinationFunction(Operator.Add)
      end
      if suit.Button("-", 750, 110, 40, 40).hit then
        self.controller:addCombinationFunction(Operator.Subtract)
      end
      if suit.Button("*", 750, 160, 40, 40).hit then
        self.controller:addCombinationFunction(Operator.Multiply)
      end
      if suit.Button("/", 750, 210, 40, 40).hit then
        self.controller:addCombinationFunction(Operator.Divide)
      end
      if suit.Button("o", 750, 260, 40, 40).hit then
        return self.controller:addCombinationFunction(Operator.Compose)
      end
    end,
    devButtons = function(self)
      if suit.Button("Save level", 560, 560, 100, 30).hit then
        return self.controller:dumpLevelToFile("dump")
      end
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, world, controller)
      self.world, self.controller = world, controller
      self.hasMouse = false
    end,
    __base = _base_0,
    __name = "GUI"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  GUI = _class_0
end
return GUI
