local Util = require("util/util")
local plainImagePool = { }
local selectedImagePool = { }
local depImagePool = { }
local buttonSize = 40
local fButtonImage, fSelectedButtonImage, fDepButtonImage, createBorderImage, Artist
fButtonImage = function(col)
  local fromPool = plainImagePool[col]
  if fromPool ~= nil then
    return fromPool
  else
    local img = Util.createImage(buttonSize, buttonSize, col)
    plainImagePool[col] = img
    return img
  end
end
fSelectedButtonImage = function(col)
  local fromPool = selectedImagePool[col]
  if fromPool ~= nil then
    return fromPool
  else
    local img = createBorderImage(col, Util.col.white, 4)
    selectedImagePool[col] = img
    return img
  end
end
fDepButtonImage = function(col)
  local fromPool = depImagePool[col]
  if fromPool ~= nil then
    return fromPool
  else
    local img = createBorderImage(col, Util.col.white, 2)
    depImagePool[col] = img
    return img
  end
end
createBorderImage = function(col, borderCol, border)
  border = border or 3
  local img = Util.createImage(buttonSize, buttonSize, borderCol)
  local data = img:getData()
  for x = 0, buttonSize - 1 do
    for y = 0, buttonSize - border - 1 do
      data:setPixel(x, y, unpack(col))
    end
  end
  img:refresh()
  return img
end
Artist = {
  fButtonImage = fButtonImage,
  fSelectedButtonImage = fSelectedButtonImage,
  fDepButtonImage = fDepButtonImage,
  createBorderImage = createBorderImage
}
return Artist
