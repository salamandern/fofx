local Util = require("util/util")
local Point = require("model/point")
local lg = love.graphics
local world = nil
local font = nil
local scrMid = nil
local scrDim = nil
local cam = nil
local camScale = nil
local notchH = 5
local render, drawWorld, updateDrawInvariants, drawBackground, drawAxes, drawNotch, drawDetectors, drawFunctions, drawStatus, drawWinText, drawHint, drawFunction, drawDebugPoints, drawLine, transformToFunctionPlane, makeStuffPixelPerfect, View
render = function(currentWorld)
  updateDrawInvariants(currentWorld)
  drawBackground()
  drawWorld()
  return drawStatus()
end
drawWorld = function()
  lg.push()
  makeStuffPixelPerfect()
  drawFunctions()
  drawDetectors()
  return lg.pop()
end
updateDrawInvariants = function(currentWorld)
  world = currentWorld
  font = lg.getFont()
  scrDim = Point(lg.getWidth(), lg.getHeight())
  scrMid = scrDim / 2
  cam = currentWorld.camera
  camScale = cam:coordinateScale()
end
drawBackground = function()
  drawDebugPoints()
  return drawAxes()
end
drawAxes = function()
  lg.setLineStyle("rough")
  lg.setColor(Util.col.darkgray)
  lg.setLine(100, "rough")
  lg.line(0, scrMid.y, scrDim.x, scrMid.y)
  lg.line(scrMid.x, 0, scrMid.x, scrDim.y)
  lg.setColor(Util.col.gray)
  for i = 0.5, math.max(cam.hdim:coords()), 0.5 do
    drawNotch(i, true)
    drawNotch(-i, true)
    drawNotch(i, false)
    drawNotch(-i, false)
  end
end
drawNotch = function(val, horizontal)
  local a, b, scrVal = nil, nil, nil
  if horizontal then
    scrVal = val * camScale.x
    a = Point(scrVal, 0)
    b = Point(scrVal, notchH)
  else
    scrVal = val * camScale.y
    a = Point(0, scrVal)
    b = Point(notchH, scrVal)
  end
  a = a + scrMid
  b = b + scrMid
  lg.setColor(Util.col.darkgray)
  drawLine(a, b)
  lg.setColor(Util.col.gray)
  return lg.print(tostring(val), b.x, b.y)
end
drawDetectors = function()
  for _, d in pairs(world.detectors) do
    lg.setColor(d:isSatisfied() and Util.col.green or Util.col.red)
    local spanDelta = Point(0, d.span)
    local a = world.camera:worldToScreen(d.pos + spanDelta)
    local b = world.camera:worldToScreen(d.pos - spanDelta)
    lg.circle("fill", a.x, a.y, 3, 10)
    lg.circle("fill", b.x, b.y, 3, 10)
    drawLine(a, b)
  end
end
drawFunctions = function()
  love.graphics.push()
  lg.setLineStyle("smooth")
  lg.setLineWidth(100000000)
  lg.setLineJoin("bevel")
  transformToFunctionPlane()
  Util.forEach(world.functions, drawFunction)
  return love.graphics.pop()
end
drawStatus = function()
  if world.winTimer then
    drawWinText()
  end
  return drawHint()
end
drawWinText = function()
  lg.setColor(Util.col.green)
  lg.setLineWidth(1)
  return lg.print("You beat the level, congrats!", 100, 100)
end
drawHint = function()
  if world.hint then
    lg.setColor(Util.col.white)
    lg.setLineWidth(1)
    return lg.print(world.hint.text, 100, 70)
  end
end
drawFunction = function(f)
  lg.setColor(f.color)
  local resolution = lg.getWidth()
  local x0 = cam.pos.x - cam.hdim.x
  local interval = cam.hdim.x * 2
  local step = interval / resolution
  local pts = { }
  for x = x0, x0 + interval, step do
    table.insert(pts, x)
    table.insert(pts, f(x))
  end
  return love.graphics.line(pts)
end
drawDebugPoints = function()
  lg.setColor(Util.col.white)
  lg.setPointSize(3)
  return lg.points({
    {
      -1,
      1
    },
    {
      0,
      1
    },
    {
      1,
      1
    },
    {
      -1,
      0
    },
    {
      0,
      0
    },
    {
      1,
      0
    },
    {
      -1,
      -1
    },
    {
      0,
      -1
    },
    {
      1,
      -1
    },
    {
      0.7,
      1
    },
    {
      0.9,
      1
    },
    {
      1,
      0.7
    },
    {
      1,
      0.9
    }
  })
end
drawLine = function(a, b)
  return lg.line(a.x, a.y, b.x, b.y)
end
transformToFunctionPlane = function()
  lg.translate(scrMid:coords())
  lg.scale(camScale:coords())
  return lg.translate((-world.camera.pos):coords())
end
makeStuffPixelPerfect = function()
  return lg.translate(-0.5, -0.5)
end
View = {
  render = render,
  updateDrawInvariants = updateDrawInvariants,
  drawBackground = drawBackground,
  drawAxes = drawAxes,
  drawNotch = drawNotch,
  drawDetectors = drawDetectors,
  drawFunctions = drawFunctions,
  drawStatus = drawStatus,
  drawFunction = drawFunction,
  drawDebugPoints = drawDebugPoints,
  drawLine = drawLine,
  transformToFunctionPlane = transformToFunctionPlane,
  makeStuffPixelPerfect = makeStuffPixelPerfect
}
return View
