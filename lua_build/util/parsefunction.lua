local lpeg = require("lpeg")
local Function = require("model/function")
local Operator = require("model/operator")
local Util = require("util/util")
local moon = require("moon")
local integer = lpeg.R("09") ^ 1
local float1 = lpeg.P("-") ^ -1 * integer * (lpeg.P(".") * integer) ^ -1
local fl1 = float1 / tonumber
local minus = lpeg.P("-")
integer = lpeg.R("09") ^ 1
local dot = lpeg.P(".")
minus = lpeg.P("-")
local float_str = minus ^ -1 * ((integer * dot * integer) + (dot * integer) + integer)
local float = float_str / tonumber
local optfloat = float ^ -1
local float_or_0 = float + lpeg.Cc(0)
local float_or_1 = float + lpeg.Cc(1)
local en = -lpeg.P(1)
local ws = lpeg.P(" ")
local wso = ws ^ 0
local comma = wso * lpeg.P(",") * wso
local lparen = lpeg.P("(") * wso
local rparen = wso * lpeg.P(")")
local tuple_capt = lpeg.Ct(lparen * float_or_1 * comma * float_or_0 * rparen) + lpeg.Cc({
  1,
  0
})
local function_name_str = lpeg.P("id") + "sin" + "cos" + "tan" + "asin" + "acos" + "atan" + "abs" + "ceil" + "floor" + "log10" + "log" + "sqrt"
local function_name = function_name_str / (function(nm)
  return {
    "fun",
    nm
  }
end)
local op_name_str = lpeg.S("+-*/o")
local op_name = op_name_str / (function(op)
  return {
    "op",
    op
  }
end)
local tokenobj
tokenobj = function(arr)
  return {
    type = arr[2][1],
    name = arr[2][2],
    yStretch = arr[1][1],
    yOffset = arr[1][2],
    xStretch = arr[3][1],
    xOffset = arr[3][2]
  }
end
local func = tuple_capt * function_name * tuple_capt
func = lpeg.Ct(func) / tokenobj
local operator = tuple_capt * op_name * tuple_capt
operator = lpeg.Ct(operator) / tokenobj
local token = func + operator
local pat = lpeg.P({
  "exp",
  exp = lpeg.Ct(func) + lpeg.V("op"),
  op = lpeg.Ct(operator * ws * lpeg.V("exp") * ws * lpeg.V("exp"))
})
local parseTokens
parseTokens = function(tokens)
  local curr = tokens[1]
  local ret
  local _exp_0 = curr.type
  if "fun" == _exp_0 then
    ret = Function[curr.name]:copy()
    ret = ret
  elseif "op" == _exp_0 then
    assert(curr.type == "op")
    local opObj
    local _exp_1 = curr.name
    if "+" == _exp_1 then
      opObj = Operator.Add
    elseif "-" == _exp_1 then
      opObj = Operator.Subtract
    elseif "*" == _exp_1 then
      opObj = Operator.Multiply
    elseif "/" == _exp_1 then
      opObj = Operator.Divide
    elseif "o" == _exp_1 then
      opObj = Operator.Compose
    else
      opObj = error("Bad op name" .. curr.name)
    end
    ret = Function:combine(opObj, parseTokens(tokens[2]), parseTokens(tokens[3]))
    ret = ret
  else
    ret = error("Bad token type " .. curr.type)
  end
  ret.yStretch = curr.yStretch
  ret.yOffset = curr.yOffset
  ret.xStretch = curr.xStretch
  ret.xOffset = curr.xOffset
  return ret
end
return function(str)
  local tokens = (wso * pat * wso * en):match(str)
  if nil ~= tokens then
    local ret, res = pcall(parseTokens, tokens)
    if ret then
      return res
    else
      print(res)
      return nil
    end
  end
  return nil
end
