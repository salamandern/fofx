local pl = (require("pl.import_into"))()
local isPrefix
isPrefix = function(string, prefix)
  return string.sub(string, 1, string.len(prefix)) == prefix
end
local isSuffix
isSuffix = function(string, suffix)
  return suffix == '' or string.sub(string, -string.len(suffix)) == suffix
end
local isYamlFile
isYamlFile = function(fileName)
  return isSuffix(fileName, ".yaml") or isSuffix(fileName, ".yml")
end
local shallowCopyKeepMt
shallowCopyKeepMt = function(orig)
  local orig_type = type(orig)
  local copy = nil
  if orig_type == 'table' then
    copy = { }
    for orig_key, orig_value in pairs(orig) do
      copy[orig_key] = orig_value
    end
    setmetatable(copy, getmetatable(orig))
  else
    copy = orig
  end
  return copy
end
local deepCopyKeepMt
deepCopyKeepMt = function(orig)
  local orig_type = type(orig)
  local copy = nil
  if orig_type == 'table' and not orig._dontcopy then
    copy = { }
    for orig_key, orig_value in next,orig,nil do
      copy[deepCopyKeepMt(orig_key)] = deepCopyKeepMt(orig_value)
    end
    setmetatable(copy, getmetatable(orig))
  else
    copy = orig
  end
  return copy
end
local shallowEqualArrays
shallowEqualArrays = function(as, bs)
  return pl.tablex.compare(as, bs, pl.operator.eq)
end
local forEach
forEach = function(array, f)
  for _, v in ipairs(array) do
    f(v)
  end
end
local map
map = function(func, array)
  local new_array = { }
  for i, v in ipairs(array) do
    new_array[i] = func(v)
  end
  return new_array
end
local empty
empty = function(arr)
  return #arr == 0
end
local evalWith
evalWith = function(x)
  return function(f)
    return f(x)
  end
end
local compose
compose = function(f, g)
  return function(x)
    return f(g(x))
  end
end
local do_if_number
do_if_number = function(subject, callback)
  local num = tonumber(subject)
  if num ~= nil then
    return callback(num)
  end
end
local overrideEntries
overrideEntries = function(subject, entries)
  for k, v in pairs(entries) do
    subject[k] = v
  end
end
local createImage
createImage = function(w, h, col)
  local data = love.image.newImageData(w, h)
  for x = 0, w - 1 do
    for y = 0, h - 1 do
      data:setPixel(x, y, unpack(col))
    end
  end
  return love.graphics.newImage(data)
end
local between
between = function(a, x, b)
  return (a <= x and x <= b) or (a >= x and x >= b)
end
local randcolor
randcolor = function()
  return {
    math.random(50, 255),
    math.random(50, 255),
    math.random(50, 255),
    255
  }
end
local col = {
  red = {
    255,
    0,
    0,
    255
  },
  green = {
    0,
    255,
    0,
    255
  },
  blue = {
    0,
    0,
    255,
    255
  },
  brown = {
    255,
    255,
    0,
    255
  },
  purple = {
    255,
    0,
    255,
    255
  },
  cyan = {
    0,
    255,
    255,
    255
  },
  white = {
    255,
    255,
    255,
    255
  },
  black = {
    0,
    0,
    0,
    255
  },
  lightgray = {
    200,
    200,
    200,
    255
  },
  gray = {
    120,
    120,
    120,
    255
  },
  darkgray = {
    50,
    50,
    50,
    255
  }
}
local Util = {
  shallowCopyKeepMt = shallowCopyKeepMt,
  deepCopyKeepMt = deepCopyKeepMt,
  isPrefix = isPrefix,
  isSuffix = isSuffix,
  isYamlFile = isYamlFile,
  shallowEqualArrays = shallowEqualArrays,
  forEach = forEach,
  map = map,
  empty = empty,
  evalWith = evalWith,
  compose = compose,
  do_if_number = do_if_number,
  overrideEntries = overrideEntries,
  createImage = createImage,
  between = between,
  randcolor = randcolor,
  col = col
}
return Util
