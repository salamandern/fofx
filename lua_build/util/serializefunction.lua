local tuple
tuple = function(s, o)
  if s == 1 and o == 0 then
    return ""
  elseif s == 1 then
    return "(," .. tostring(o) .. ")"
  elseif o == 0 then
    return "(" .. tostring(s) .. ",)"
  else
    return "(" .. tostring(s) .. "," .. tostring(o) .. ")"
  end
end
local tokenize
tokenize = function(t)
  local nm = t.operator and t.operator.name or t.name
  return tuple(t.yStretch, t.yOffset) .. nm .. tuple(t.xStretch, t.xOffset)
end
local serialize
serialize = function(f)
  local op = f.operator
  if op then
    return tostring(tokenize(f)) .. " " .. tostring(serialize(f.parts[1])) .. " " .. tostring(serialize(f.parts[2]))
  else
    return tokenize(f)
  end
end
return serialize
