local moon = require("moon")
local pl = require("pl.import_into")()
local YAML = require("yaml")
local fun = require("fun")
local Util = require("util/util")
local Level = require("model/level")
local Function = require("model/function")
local Detector = require("model/detector")
local Point = require("model/point")
local serf = require("util/serializefunction")
local parf = require("util/parsefunction")
local serialize
serialize = function(f)
  local spec = {
    functions = Util.map(serf, f.functions),
    detectors = f.detectors,
    functionBank = Util.map(serf, f.functionBank)
  }
  local ret = YAML.dump(spec)
  return ret
end
local makeDetector
makeDetector = function(spec)
  local pos = Point(spec.pos.x, spec.pos.y)
  return Detector(pos, spec.shouldOverlap, spec.span)
end
local isValidSpec
isValidSpec = function(spec)
  return type(spec) == "table" and type(spec.functions) == "table" and type(spec.detectors) == "table" and type(spec.functionBank) == "table"
end
local deserialize
deserialize = function(str)
  if type(str) == "string" then
    local spec = YAML.load(str)
    if isValidSpec(spec) then
      spec.functions = pl.tablex.imap(parf, spec.functions)
      spec.detectors = pl.tablex.imap(makeDetector, spec.detectors)
      spec.functionBank = pl.tablex.imap(parf, spec.functionBank)
      return Level.fromSpec(spec)
    else
      return nil
    end
  else
    return nil
  end
end
return {
  serialize = serialize,
  deserialize = deserialize
}
