moon = require "moon"
test = require "lib/gambiarra"
serializeLevel = require "util/serializelevel"
Level = require "model/level"
Function = require "model/function"
Detector = require "model/detector"
Operator = require "model/operator"
Point = require "model/point"
serl = serializeLevel.serialize
deserl = serializeLevel.deserialize

test "Basics", ->
  spec = {
    functions: {},
    detectors: {},
    functionBank: {},
  }
  emptyLevel = Level.fromSpec(spec)
  spec = {
    functions: {Function.sin\copy!},
    detectors: {Detector!},
    functionBank: {Function.cos\copy!},
  }
  oneLevel = Level.fromSpec(spec)
  spec = {
    functions: {Function.sin\copy!, Function.cos\copy!},
    detectors: {Detector!, Detector(Point(3,2))},
    functionBank: {Function.cos\copy!, Function.sin\copy!},
  }
  manyLevel = Level.fromSpec(spec)

  emptyAsStr = serl(emptyLevel)
  oneAsStr = serl(oneLevel)
  manyAsStr = serl(manyLevel)

  ok type(emptyAsStr) == "string", "emtpy Serializes to a string"
  ok type(oneAsStr) == "string", " one Serializes to a string"
  ok type(manyAsStr) == "string", "many Serializes to a string"

  ok emptyLevel == deserl(emptyAsStr), "empty Deserializes back to the same"
  ok oneLevel == deserl(oneAsStr), "one Deserializes back to the same"
  ok manyLevel == deserl(manyAsStr), "many Deserializes back to the same"

  ok emptyLevel != deserl(oneAsStr), "one not equal to empty"
  ok emptyLevel != deserl(manyAsStr), "many not equal to empty"

test "RealLife", ->
  someLevels = {
    {
      functions: {
        Function\combine(Operator.Add, Function.sin\copy!, Function\combine(Operator.Multiply, Function.cos\copy!, Function.tan\copy!)),
      },
      detectors: {
        Detector(Point(-1.19, 0.789), true, 0.1),
        Detector(Point(0.72, 0.75), false, 0.1),
        Detector(Point(-2.14, 0.75), true, 0.1),
        Detector(Point(1.66, 0.80), false, 0.1),
      },
      functionBank: {
        Function.sin,
        Function.cos,
      },
    },
    {
      functions: { },
      detectors: {
        Detector(Point(-2.9, -0.229)),
        Detector(Point(-2.579, -0.532)),
        Detector(Point(-2.248, -0.779)),
        Detector(Point(0.423, 0.40757620510)),
        Detector(Point(0.753, 0.68273768676)),
        Detector(Point(1.083, 0.88335038412)),
        Detector(Point(1.4129999999999, 0.985218139931)),
        Detector(Point(-3.062999999999, -7.8511769988), false),
      },
      functionBank: {
        Function.sin,
      },
    },
    {
      functions: { },
      detectors: {
        Detector(Point(-1, 0), false, 0.8),
        Detector(Point(-0.8, 0)),
        Detector(Point(-0.5, 0)),
        Detector(Point(-0.2, 0)),
        Detector(Point(0, 0), false, 0.8),
        Detector(Point(0.2, 0)),
        Detector(Point(0.5, 0)),
        Detector(Point(0.8, 0)),
        Detector(Point(1, 0), false, 0.8),
      },
      functionBank: {
        Function.sin,
        Function.abs,
      }
    }
  }
  lvl1 = Level.fromSpec(someLevels[1])
  lvl2 = Level.fromSpec(someLevels[2])
  lvl3 = Level.fromSpec(someLevels[3])

  str1 = serl(lvl1)
  str2 = serl(lvl2)
  str3 = serl(lvl3)

  delvl1 = deserl(str1)
  delvl2 = deserl(str2)
  delvl3 = deserl(str3)

  ok lvl1 == delvl1, "lvl1 deserl to self"
  ok lvl2 == delvl2, "lvl2 deserl to self"
  ok lvl3 == delvl3, "lvl3 deserl to self"

  ok Level! != delvl1, "delvl1 nonequal to empty"
  ok Level! != delvl2, "delvl2 nonequal to empty"
  ok Level! != delvl3, "delvl3 nonequal to empty"


