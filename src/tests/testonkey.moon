test = require "lib/gambiarra"
OnKey = require "controller/onkey"

test("Onkey general", () ->
  ref = {x: 1}
  sub = OnKey()
  sub\add{a: () -> ref.x = 2}
  sub\run("b")
  eqok(ref.x, 1, "Does not trigger on wrong key")

  sub\run("a")
  eqok(ref.x, 2, "Does trigger on right key")

  sub\add{b: () -> ref.x = 3}
  sub\run("b")
  eqok(ref.x, 3, "Adding second works")

  sub\add{b: () -> ref.x = 4}
  sub\run("b")
  eqok(ref.x, 4, "Overriding")

  sub\run("a")
  eqok(ref.x, 2, "Overriding does not disturb others")
)

test("Separation", () ->
  ref = {x: 1}
  sub1 = OnKey()
  sub2 = OnKey()
  sub1\add{a: () -> ref.x = 2}
  sub2\run("a")
  eqok(ref.x, 1, "OnKeys are separate")
)
