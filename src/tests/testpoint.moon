test = require "lib/gambiarra"
Point = require "model/point"

test("Constructor", () ->
  p = Point()
  ok(p.x == 0 and p.y == 0, "Zero-initialized")
  p = Point(3, -4)
  ok(p.x == 3 and p.y == -4, "Parameters")
)

test("Math", () ->
  minusOne = Point(-1, -1)
  zero = Point()
  one = Point(1, 1)
  two = Point(2, 2)
  three = Point(3, 3)
  four = Point(4, 4)

  ok(eq(one+two, three), "point addition")
  ok(eq(one+2, three), "postfix number addition")
  ok(eq(2+one, three), "prefix number addition")
  ok(eq(one+one, two), "adding to yourself")

  ok(eq(three-two, one), "point subtraction")
  ok(eq(three-2, one), "postfix number subtraction")
  ok(eq(3-two, one), "prefix number subtraction")
  ok(eq(one-one, zero), "subtracting yourself")

  ok(eq(one*two, two), "point multiplication")
  ok(eq(one*2, two), "postfix number multiplication")
  ok(eq(2*one, two), "prefix number multiplication")
  ok(eq(two*two, four), "multiplying yourself")

  ok(eq(four/two, two), "point division")
  ok(eq(four/2, two), "postfix number division")
  ok(eq(4/two, two), "prefix number division")
  ok(eq(two/two, one), "dividing yourself (deep...)")

  ok(eq(-one, minusOne), "unary minus")

  ok(two\inside(one, three), "point inside")
  ok(not four\inside(one, three), "point not inside")
  ok(three\inside(one, three), "point at edge B should be inside")
  ok(one\inside(one, three), "point at edge A should be inside")
)

test "Equality", ->
  ok Point! == Point!, "Empty constructor"
  ok Point(0,0) == Point!, "Empty right"
  ok Point! == Point(0,0), "Empty left"
  ok Point(1,-2) == Point(1,-2), "value constructors"
  ok Point! == Point!, "Empty constructor"

  ok not (Point! == Point(1, 2)), "Empty left vs values"
  ok not (Point(1, 2) == Point!), "Empty right vs values"
  ok not (Point(1, 2) == Point(1, 3)), "y differs"
  ok not (Point(2, 2) == Point(1, 2)), "x differs"

  ok Point(2, 2) != Point(1, 2), "!= operator"
  ok not (Point(2, 2) != Point(2, 2)), "!= operator"

test("Misc", () ->
  three = Point(3, 3)
  x, y = three\coords()
  ok(x == 3 and y == 3, "Coords extraction")

  strTwo = tostring(three)
  ok("(3,3)" == strTwo, "tostring")
)
