test = require "lib/gambiarra"
Model = require "model/model"

test("Worlds separate", () ->
  w1 = Model()
  w2 = Model()
  w1.time = 1337
  ok(w1.time ~= w2.time, "Changing var does not change var")
  w1.camera.x = 13
  ok(w1.camera.x ~= w2.camera.x, "Camera")
)
