export love = {filesystem: {}}

test = require "lib/gambiarra"
Util = require "util/util"
Level = require "model/level"
Function = require "model/function"
Detector = require "model/detector"
Point = require "model/point"
serializeLevel = require "util/serializelevel"
serl = serializeLevel.serialize
deserl = serializeLevel.deserialize
loadlevel = require "controller/loadlevel"

test "AvailableLevels", ->
  dirContentsSpy = spy((dir) -> {})
  isDirSpy = spy(-> false)
  love.filesystem.getDirectoryItems = dirContentsSpy
  love.filesystem.isDirectory = isDirSpy

  eqok loadlevel.availableLevels!, 0, "No levels"

  eqok #dirContentsSpy.called, 1, "Calls getDIrectoryContents"
  whereChecked = dirContentsSpy.called[1] and dirContentsSpy.called[1][1]
  eqok whereChecked, "levels", "Checks in right directory"
  eqok #isDirSpy.called, 0, "Does not check if dirs, nothing to check"
  
  dirContentsSpy = spy((dir) -> {"qwe"})
  isDirSpy = spy((str) -> string.len(str) >= 5)
  love.filesystem.getDirectoryItems = dirContentsSpy
  love.filesystem.isDirectory = isDirSpy

  eqok loadlevel.availableLevels!, 1, "One level"

  eqok #dirContentsSpy.called, 1, "Calls getDirectoryContents"
  whereChecked = dirContentsSpy.called[1] and dirContentsSpy.called[1][1]
  eqok whereChecked, "levels", "Checks in right directory"
  eqok #isDirSpy.called, 1, "Checks if directory"
  eqok (isDirSpy.called[1] and isDirSpy.called[1][1]), "qwe", "Checks for the actual file lol"

  dirContentsSpy = spy((dir) -> {"thisisadirectory"})
  isDirSpy = spy((str) -> string.len(str) >= 5)
  love.filesystem.getDirectoryItems = dirContentsSpy
  love.filesystem.isDirectory = isDirSpy

  eqok loadlevel.availableLevels!, 0, "One level"

  eqok #dirContentsSpy.called, 1, "Calls getDirectoryContents"
  whereChecked = dirContentsSpy.called[1] and dirContentsSpy.called[1][1]
  eqok whereChecked, "levels", "Checks in right directory"
  eqok #isDirSpy.called, 1, "Checks if directory"
  eqok (isDirSpy.called[1] and isDirSpy.called[1][1]), "thisisadirectory", "Checks for the actual file lol"

  dirContentsSpy = spy((dir) -> {"qwe", "ewq", "directory", "rew"})
  isDirSpy = spy((str) -> string.len(str) >= 5)
  love.filesystem.getDirectoryItems = dirContentsSpy
  love.filesystem.isDirectory = isDirSpy

  eqok loadlevel.availableLevels!, 3, "One level"

  eqok #dirContentsSpy.called, 1, "Calls getDirectoryContents"
  whereChecked = dirContentsSpy.called[1] and dirContentsSpy.called[1][1]
  eqok whereChecked, "levels", "Checks in right directory"
  eqok #isDirSpy.called, 4, "Checks all if directory"
  eqok (isDirSpy.called[1] and isDirSpy.called[1][1]), "qwe", "Checks for the actual file lol"
  eqok (isDirSpy.called[2] and isDirSpy.called[2][1]), "ewq", "Checks for the actual file lol"
  eqok (isDirSpy.called[3] and isDirSpy.called[3][1]), "directory", "Checks for the actual file lol"
  eqok (isDirSpy.called[4] and isDirSpy.called[4][1]), "rew", "Checks for the actual file lol"

allLevels = {
  {
    functions: {
      Function.cool\copy({color: Util.col.red, yStretch: 0, xStretch: 6}),
    },
    detectors: {
      Detector(Point(-1.19, 0.789), true, 0.1),
      Detector(Point(0.72, 0.75), false, 0.1),
      Detector(Point(-2.14, 0.75), true, 0.1),
      Detector(Point(1.66, 0.80), false, 0.1),
    },
    functionBank: {
      Function.sin,
      Function.cos,
    },
  }, {
    functions: { },
    detectors: {
      Detector(Point(-2.9, -0.229)),
      Detector(Point(-2.579, -0.532)),
      Detector(Point(-2.248, -0.779)),
      Detector(Point(-1.916, -0.941)),
      Detector(Point(-1.583, -1)),
      Detector(Point(-1.251, -0.949)),
      Detector(Point(-0.919, -0.794)),
      Detector(Point(-0.588, -0.553)),
      Detector(Point(-0.26, -0.253)),
      Detector(Point(0, 0.15, false)),
      Detector(Point(0, -0.15, false)),
      Detector(Point(0.423, 0.4075646)),
      Detector(Point(0.753, 0.682755)),
      Detector(Point(1.083, 0.883341)),
      Detector(Point(1.412998, 0.986822)),
      Detector(Point(1.742997, 0.978879)),
      Detector(Point(2.072995, 0.82111)),
      Detector(Point(2.402996, 0.671472)),
      Detector(Point(2.732997, 0.311012)),
      Detector(Point(3.062997, 0.05578301)),
      Detector(Point(0.26, 0.25518921551), false),
      Detector(Point(0.58, 0.5529928), false),
      Detector(Point(0.91, 0.7981099999), false),
      Detector(Point(1.24, 0.941489998), false),
      Detector(Point(1.57, 0.9975599996), false),
      Detector(Point(1.89, 0.9487699995), false),
      Detector(Point(2.22, 0.7915699993), false),
      Detector(Point(2.55, 0.55779999), false),
      Detector(Point(2.88, 0.257379999), false),
      Detector(Point(-0.423, -0.4194613), false),
      Detector(Point(-0.753, -0.682377), false),
      Detector(Point(-1.083, -0.883376), false),
      Detector(Point(-1.412998, -0.909227), false),
      Detector(Point(-1.742997, -0.965166), false),
      Detector(Point(-2.072995, -0.855502), false),
      Detector(Point(-2.402996, -0.60765), false),
      Detector(Point(-2.732997, -0.352544), false),
      Detector(Point(-3.062997, -7.84072), false),
    },
    functionBank: {
      Function.sin,
    },
  }, {
    functions: { },
    detectors: {
      Detector(Point(-1, 0), false, 0.8),
      Detector(Point(-0.8, 0)),
      Detector(Point(-0.5, 0)),
      Detector(Point(-0.2, 0)),
      Detector(Point(0, 0), false, 0.8),
      Detector(Point(0.2, 0)),
      Detector(Point(0.5, 0)),
      Detector(Point(0.8, 0)),
      Detector(Point(1, 0), false, 0.8),
    },
    functionBank: {
      Function.sin,
      Function.abs,
    },
  }
}

test "Serve", ->
  love.filesystem.getDirectoryItems = (dir) -> return {"level0", "level1", "level2", "level3"}
  love.filesystem.isDirectory = -> false

  ok nil == loadlevel.serve(0), "nil served at 0"
  pastEndId = loadlevel.availableLevels! + 1
  ok nil == loadlevel.serve(pastEndId), "nil served at past end id"

  level1 = Level.fromSpec(allLevels[1])
  level2 = Level.fromSpec(allLevels[2])
  level3 = Level.fromSpec(allLevels[3])

  readSpy = spy (path) ->
    switch path
      when "levels/level0"
        serl(Level!)
      when "levels/level1"
        serl(level1)
      when "levels/level2"
        serl(level2)
      when "levels/level3"
        serl(level3)
      else
        error "No such level #{path}"

  love.filesystem.getDirectoryItems = (dir) -> return {"level0", "level1", "level2", "level3"}
  love.filesystem.isDirectory = -> false
  love.filesystem.read = readSpy

  served = loadlevel.serve(1)
  ok served == Level!, "Deserializes to appropriate level"
  eqok served.levelMetadata and served.levelMetadata.id, 1, "Correct metadata"
  eqok #readSpy.called, 1, "Reads file"
  eqok readSpy.called[1] and readSpy.called[1][1], "levels/level0", "Reads correct file"

  readSpy.called = {}

  served = loadlevel.serve(2)
  ok served == level1, "Deserializes to appropriate level"
  eqok served.levelMetadata and served.levelMetadata.id, 2, "Correct metadata"
  eqok #readSpy.called, 1, "Reads file"
  eqok readSpy.called[1] and readSpy.called[1][1], "levels/level1", "Reads correct file"

  readSpy.called = {}
  
  served = loadlevel.serve(3)
  ok served == level2, "Deserializes to appropriate level"
  eqok served.levelMetadata and served.levelMetadata.id, 3, "Correct metadata"
  eqok #readSpy.called, 1, "Reads file"
  eqok readSpy.called[1] and readSpy.called[1][1], "levels/level2", "Reads correct file"

  readSpy.called = {}

  served = loadlevel.serve(4)
  ok served == level3, "Deserializes to appropriate level"
  eqok served.levelMetadata and served.levelMetadata.id, 4, "Correct metadata"
  eqok #readSpy.called, 1, "Reads file"
  eqok readSpy.called[1] and readSpy.called[1][1], "levels/level3", "Reads correct file"

test "Serve: ordering", ->
  justCreatedPath = ""

  love.filesystem.getDirectoryItems = (dir) -> {"b", "a"}
  love.filesystem.isDirectory = -> false
  love.filesystem.read = (path) ->
    justCreatedPath = path
    serl Level!

  loadlevel.serve(1)
  eqok justCreatedPath, "levels/a", "alphabetical oerder"
  loadlevel.serve(2)
  eqok justCreatedPath, "levels/b", "alphabetical oerder"

test "Serve: skip dirs", ->
  justCreatedPath = ""

  love.filesystem.getDirectoryItems = (dir) -> {"a", "b"}
  love.filesystem.isDirectory = (nm) -> nm == "a"
  love.filesystem.read = (path) ->
    justCreatedPath = path
    serl Level!

  loadlevel.serve(1)
  eqok justCreatedPath, "levels/b", "Skipped first one because was directory"

