export love = {graphics: {}}

test = require "lib/gambiarra"
Point = require "model/point"
Camera = require "model/camera"
assumeHH = 2
assumeHW = 2.8
assumeDims = Point(assumeHW, assumeHH)*2
windowDims = Point(800, 600)
screenMid = windowDims / 2

test "Construction", () ->
  cam = Camera(3, 4)
  ok(cam.hdim.y == assumeHH and cam.hdim.x == assumeHW, "No flexibility for yuo!")
  ok(cam.pos.x == 3 and cam.pos.y == 4, "Point constructor")


test("Scaling", () ->
  dimSpy = spy(() -> windowDims.x, windowDims.y )
  widSpy = spy(() -> windowDims.x)
  heiSpy = spy(() -> windowDims.y)
  love.graphics.getDimensions = dimSpy
  love.graphics.getWidth = widSpy
  love.graphics.getHeight = heiSpy
  cam = Camera()

  eqok(cam\coordinateScale(), Point(1, -1)*windowDims/assumeDims, "Correct scale")
  ok(dimSpy.called or widSpy.called and heiSpy.called, "Asks löve for info")
  eqok(cam\worldToScreen(Point()), screenMid, "Origin in center")

  unitCircle = {
    Point(1, 0),
    Point(0, 1),
    Point(-1, 0),
    Point(0, -1),
  }

  screenUnitCircle = {
    screenMid + Point( screenMid.x/assumeHW, 0),
    screenMid + Point(0, -screenMid.y/assumeHH),
    screenMid + Point(-screenMid.x/assumeHW, 0),
    screenMid + Point(0,  screenMid.y/assumeHH),
  }

  for i, p in ipairs(unitCircle)
    eqok(tostring(cam\worldToScreen(p)), tostring(screenUnitCircle[i]), "UnitC world to screen")

  for i, p in ipairs(screenUnitCircle)
    eqok(tostring(cam\screenToWorld(p)), tostring(unitCircle[i]), "UnitC screenToWorld")
)
