test = require "lib/gambiarra"
moon = require "moon"
Function = require "model/function"
Operator = require "model/operator"
parse = require "util/parsefunction"

test "Plain functions", ->
  ok parse("id") == Function.id\copy!, "id"
  ok parse("sin") == Function.sin\copy!, "sin"
  ok parse("cos") == Function.cos\copy!, "cos"
  ok parse("tan") == Function.tan\copy!, "tan"
  ok parse("asin") == Function.asin\copy!, "asin"
  ok parse("acos") == Function.acos\copy!, "acos"
  ok parse("atan") == Function.atan\copy!, "atan"
  ok parse("abs") == Function.abs\copy!, "abs"
  ok parse("ceil") == Function.ceil\copy!, "ceil"
  ok parse("floor") == Function.floor\copy!, "floor"
  ok parse("log") == Function.log\copy!, "log"
  ok parse("log10") == Function.log10\copy!, "log10"
  ok parse("sqrt") == Function.sqrt\copy!, "sqrt"

test "whitespace allowed", ->
  ok parse("    id") == Function.id\copy!, "function Space before"
  ok parse("id   ") == Function.id\copy!, "function Space after"
  ok parse("   id   ") == Function.id\copy!, "function Space on both sides"

  f = Function\combine Operator.Compose, Function.sin\copy!, Function.sin\copy!
  ok parse("      o sin sin") == f, "op Space before"
  ok parse("o sin sin ") == f, "op Space after"
  ok parse("  o sin sin   ") == f, "op Space both"

  id = Function.id\copy!
  id.yStretch = 11
  ok parse("  (11,)id(,)") == id, "Mod function space before"
  ok parse("(11,)id(,)    ") == id, "Mod function space after"
  ok parse(" (11,)id(,)   ") == id, "Mod function space both"

  f = Function.sin\copy!
  g = Function.cos\copy!
  h = Function.tan\copy!
  fg = Function\combine(Operator.Subtract, f, g)
  fgh = Function\combine(Operator.Add, fg, h)
  ok parse("   + - sin cos tan") == fgh, "Deep op space before"
  ok parse("+ - sin cos tan  ") == fgh, "Deep op space after"
  ok parse("  + - sin cos tan   ") == fgh, "Deep op space both"

test "Modifiers", ->
  id = Function.id\copy!

  ok parse("(,)id") == id, "empty left mod"
  ok parse("id(,)") == id, "empty right mod"
  ok parse("(,)id(,)") == id, "empty two mods"

  id = Function.id\copy!
  id.yStretch = 11
  ok parse("(11,)id(,)") == id, "yStretch"

  id = Function.id\copy!
  id.yOffset = 11
  ok parse("(,11)id(,)") == id, "yOffset"

  id = Function.id\copy!
  id.xStretch = 11
  ok parse("(,)id(11,)") == id, "xStretch"

  id = Function.id\copy!
  id.xOffset = 11
  ok parse("(,)id(,11)") == id, "xOffset"

  id = Function.id\copy!
  id.yStretch = 11
  id.yOffset = 12
  id.xStretch = 13
  id.xOffset = 14
  ok parse("(11,12)id(13,14)") == id, "All"

  id = Function.id\copy!
  id.yStretch = 1.1
  id.yOffset = -12
  id.xStretch = -.13
  id.xOffset = .14
  ok parse("(1.1,-12)id(-.13,.14)") == id, "All floats"

  id = Function.id\copy!
  id.yStretch = 11
  id.yOffset = 12
  ok parse("(11,12)id") == id, "full - empty"

  id = Function.id\copy!
  id.xStretch = 11
  id.xOffset = 12
  ok parse("id(11,12)") == id, "empty - full"

test "Operators names", ->
  f = Function\combine Operator.Add, Function.sin\copy!, Function.sin\copy!
  ok parse("+ sin sin") == f, "+"

  f = Function\combine Operator.Subtract, Function.sin\copy!, Function.sin\copy!
  ok parse("- sin sin") == f, "-"

  f = Function\combine Operator.Divide, Function.sin\copy!, Function.sin\copy!
  ok parse("/ sin sin") == f, "/"

  f = Function\combine Operator.Multiply, Function.sin\copy!, Function.sin\copy!
  ok parse("* sin sin") == f, "*"

  f = Function\combine Operator.Compose, Function.sin\copy!, Function.sin\copy!
  ok parse("o sin sin") == f, "o"

  ok nil == parse("# sin sin"), "# is not an op"
  ok nil == parse("^ sin sin"), "^ is not an op"
  ok nil == parse("f sin sin"), "f is not an op"
  ok nil == parse("sin sin sin"), "sin is not an op"

test "Operators cant be alone", ->
  ok nil == parse("+"), "+"
  ok nil == parse("-"), "-"
  ok nil == parse("*"), "*"
  ok nil == parse("/"), "/"
  ok nil == parse("o"), "o"

test "Operator with modifiers", ->
  f = Function.sin\copy!
  f.yStretch = 15
  f.yOffset = 16
  f.xStretch = 17
  f.xOffset = 18
  g = Function.cos\copy!
  g.yStretch = 19
  g.yOffset = 20
  g.xStretch = 21
  g.xOffset = 22
  fg = Function\combine(Operator.Multiply, f, g)
  fg.yStretch = 11
  fg.yOffset = 12
  fg.xStretch = 13
  fg.xOffset = 14
  parsed = parse("(11,12)*(13,14) (15,16)sin(17,18) (19,20)cos(21,22)")
  ok parsed == fg, "Operator with modifiers"

test "Deeply nested operators", ->
  f = Function.sin\copy!
  g = Function.cos\copy!
  h = Function.tan\copy!
  i = Function.log\copy!

  fg = Function\combine(Operator.Subtract, f, g)
  fgh = Function\combine(Operator.Add, fg, h)
  ok parse("+ - sin cos tan") == fgh, "Left branch"

  gh = Function\combine(Operator.Subtract, g, h)
  fgh = Function\combine(Operator.Add, f, gh)
  ok parse("+ sin - cos tan") == fgh, "Right branch"

  fg = Function\combine(Operator.Add, f, g)
  hi = Function\combine(Operator.Subtract, h, i)
  fghi = Function\combine(Operator.Multiply, fg, hi)
  ok parse("* + sin cos - tan log") == fghi, "Both branches"

  ff = Function\combine(Operator.Add, f, f\copy!)
  fff = Function\combine(Operator.Add, ff, f\copy!)
  ffff = Function\combine(Operator.Add, fff, f\copy!)
  fffff = Function\combine(Operator.Add, ffff, f\copy!)
  ok parse("+ + + + sin sin sin sin sin") == fffff, "Deep a f"

  f = Function\combine(Operator.Add, Function.sin\copy!, Function\combine(Operator.Multiply, Function.cos\copy!, Function.tan\copy!))
  ok parse("+ sin * cos tan") == f, "an example"

test "Nested modifiers", ->
  f = Function.sin\copy!
  g = Function.cos\copy!
  g.yStretch = 15
  h = Function.tan\copy!

  fg = Function\combine(Operator.Subtract, f, g)
  fg.yStretch = 11
  fg.yOffset = 12
  fg.xStretch = 13
  fg.xOffset = 14

  fgh = Function\combine(Operator.Add, fg, h)
  ok parse("+ (11, 12)-(13, 14) sin (15,)cos tan"), "Nested modifiers"

test "Erroneous", ->
  Function.qwe = Function(->, "qwe")
  ok nil == parse("qwe"), "qwe is not originally a function, even tho it *exists*"

  ok nil == parse("(()sin"), "extra paren in mod"
  ok nil == parse("(#)sin"), "extra sign in mod"
  ok nil == parse("()sin"), "No comma in mod"
  ok nil == parse("(1)sin"), "int but no comma"
  ok nil == parse("(1.0)sin"), "float but no comma"
  ok nil == parse("(--123,)sin"), "Fucked int"
  ok nil == parse("(1..23,)sin"), "Fucked float"
  ok nil == parse("(1.23.,)sin"), "Fucked float 2"
  ok nil == parse("(,)(,)sin"), "Extra left mod"
  ok nil == parse("(,)(,)sin(,)(,)"), "Extra both mods"
  ok nil == parse("(,)sin(,)qwe"), "Extra chars after right mod"
  ok nil == parse("qwe(,)sin(,)"), "Extra chars before left mod"

  ok nil == parse("+ sin sin sin"), "Extra function"
  ok nil == parse("+ * *"), "Ops as functions"
  ok nil == parse("+ sin *"), "Right branch op"
  ok nil == parse("+ * sin"), "Left branch op"
  ok nil == parse("sin + sin sin"), "Op after function"
  ok nil == parse("sin sin"), "Function after function"
  ok nil == parse("+ * - sin cos - tan log sin sin"), "Extra funciton nested deep af"







