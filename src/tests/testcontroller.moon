moon = require "moon"
test = require "lib/gambiarra"

pl = (require "pl.import_into")!
Model = require "model/model"
package.preload["lib/SUIT"] = -> {} -- For some reason, when GUI loads SUIT it does not work to load it.
Controller = require "controller/controller"
Level = require "model/level"
Function = require "model/function"
Operator = require "model/operator"
Detector = require "model/detector"
Point = require "model/point"
Util = require "util/util"
parseLevel = (require "util/serializelevel").deserialize
serf = require "util/serializefunction"
deserf = require "util/parsefunction"

test "DumpLevel", ->
  world = Model Level!
  cont = Controller world

  oldWorld = pl.tablex.deepcopy(world)
  dump, status = cont\dumpLevel!
  ok pl.tablex.deepcompare(oldWorld, world, false, 0.0000001), "Does not modify world"
  ok type(dump) == "string", "Dumps a string"
  parse = parseLevel(dump)
  ok Level! == parse, "Empty parses back as itself"


  someLevels = {
    {
      functions: {
        Function\combine(Operator.Add, Function.sin\copy!, Function\combine(Operator.Multiply, Function.cos\copy!, Function.tan\copy!)),
      },
      detectors: {
        Detector(Point(-1.19, 0.789), true, 0.1),
        Detector(Point(0.72, 0.75), false, 0.1),
        Detector(Point(-2.14, 0.75), true, 0.1),
        Detector(Point(1.66, 0.80), false, 0.1),
      },
      functionBank: {
        Function.sin,
        Function.cos,
      },
    },
    {
      functions: { },
      detectors: {
        Detector(Point(-2.9, -0.229)),
        Detector(Point(-2.579, -0.532)),
        Detector(Point(-2.248, -0.779)),
        Detector(Point(0.423, 0.40757620510)),
        Detector(Point(0.753, 0.68273768676)),
        Detector(Point(1.083, 0.88335038412)),
        Detector(Point(1.4129999999999, 0.985218139931)),
        Detector(Point(-3.062999999999, -7.8511769988), false),
      },
      functionBank: {
        Function.sin,
      },
    },
    {
      functions: { },
      detectors: {
        Detector(Point(-1, 0), false, 0.8),
        Detector(Point(-0.8, 0)),
        Detector(Point(-0.5, 0)),
        Detector(Point(-0.2, 0)),
        Detector(Point(0, 0), false, 0.8),
        Detector(Point(0.2, 0)),
        Detector(Point(0.5, 0)),
        Detector(Point(0.8, 0)),
        Detector(Point(1, 0), false, 0.8),
      },
      functionBank: {
        Function.sin,
        Function.abs,
      }
    }
  }
  lvl1 = Level.fromSpec(someLevels[1])
  lvl2 = Level.fromSpec(someLevels[2])
  lvl3 = Level.fromSpec(someLevels[3])

  world1 = Model lvl1
  world2 = Model lvl2
  world3 = Model lvl3

  cont1 = Controller world1
  cont2 = Controller world2
  cont3 = Controller world3

  -- Note: deepcopy simply copies the values, and does not respect methods such as \copy!
  -- As a result, deepcompare between the original and the copy will sometimes return false
  -- if you respect eq operators. So dont do that.
  oldWorld1 = pl.tablex.deepcopy(world1)
  oldWorld2 = pl.tablex.deepcopy(world2)
  oldWorld3 = pl.tablex.deepcopy(world3)


  dump1 = cont1\dumpLevel!
  dump2 = cont2\dumpLevel!
  dump3 = cont3\dumpLevel!

  ok pl.tablex.deepcompare(oldWorld1, world1, true), "real life 1 Does not modify world"
  ok pl.tablex.deepcompare(oldWorld2, world2, true), "real life 2 Does not modify world"
  ok pl.tablex.deepcompare(oldWorld3, world3, true), "real life 3 Does not modify world"

  ok type(dump1) == "string", "real life 1 Dumps a string"
  ok type(dump2) == "string", "real life 2 Dumps a string"
  ok type(dump3) == "string", "real life 3 Dumps a string"

  parse1 = parseLevel(dump1)
  parse2 = parseLevel(dump2)
  parse3 = parseLevel(dump3)

  ok lvl1 == parse1, "real life 1 parses back as itself"
  ok lvl2 == parse2, "real life 2 parses back as itself"
  ok lvl3 == parse3, "real life 3 parses back as itself"

test "AddCombinationFunction", ->
  local cont
  addcf = (op) -> cont\addCombinationFunction(op)

  cont = Controller Model Level!
  ret = pcall(addcf, "qwe")
  eqok ret, false, "Throws if given string"

  cont = Controller Model Level!
  ret = pcall(addcf, {})
  eqok ret, false, "Throws if given empty table"

  cont = Controller Model Level!
  ret = pcall(addcf, Operator.Add)
  eqok ret, true, "Does not throw if given actual operator"

  --This test suite is quite incomplete.


