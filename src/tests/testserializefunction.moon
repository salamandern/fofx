test = require "lib/gambiarra"
Function = require "model/function"
Operator = require "model/operator"
ser = require "util/serializefunction"

test "Function names", ->
  eqok ser(Function.id), "id", "id"
  eqok ser(Function.sin), "sin", "sin"
  eqok ser(Function.cos), "cos", "cos"
  eqok ser(Function.tan), "tan", "tan"
  eqok ser(Function.asin), "asin", "asin"
  eqok ser(Function.acos), "acos", "acos"
  eqok ser(Function.atan), "atan", "atan"
  eqok ser(Function.abs), "abs", "abs"
  eqok ser(Function.ceil), "ceil", "ceil"
  eqok ser(Function.floor), "floor", "floor"
  eqok ser(Function.log), "log", "log"
  eqok ser(Function.log10), "log10", "log10"
  eqok ser(Function.sqrt), "sqrt", "sqrt"
  nil

test "Operator names", ->
  eqok ser(Function\combine(Operator.Add, Function.sin, Function.cos)), "+ sin cos", "Add"
  eqok ser(Function\combine(Operator.Subtract, Function.sin, Function.cos)), "- sin cos", "Subtract"
  eqok ser(Function\combine(Operator.Multiply, Function.sin, Function.cos)), "* sin cos", "Multiply"
  eqok ser(Function\combine(Operator.Divide, Function.sin, Function.cos)), "/ sin cos", "Divide"
  eqok ser(Function\combine(Operator.Compose, Function.sin, Function.cos)), "o sin cos", "Compose"

test "Nested ops", ->
  f = Function.sin\copy!
  g = Function.cos\copy!
  h = Function.tan\copy!
  i = Function.log\copy!

  fg = Function\combine(Operator.Subtract, f, g)
  fgh = Function\combine(Operator.Add, fg, h)
  eqok ser(fgh), "+ - sin cos tan", "Left branch"

  gh = Function\combine(Operator.Subtract, g, h)
  fgh = Function\combine(Operator.Add, f, gh)
  eqok ser(fgh), "+ sin - cos tan", "Right branch"

  fg = Function\combine(Operator.Add, f, g)
  hi = Function\combine(Operator.Subtract, h, i)
  fghi = Function\combine(Operator.Multiply, fg, hi)
  eqok ser(fghi), "* + sin cos - tan log", "Both branches"

  ff = Function\combine(Operator.Add, f, f\copy!)
  fff = Function\combine(Operator.Add, ff, f\copy!)
  ffff = Function\combine(Operator.Add, fff, f\copy!)
  fffff = Function\combine(Operator.Add, ffff, f\copy!)
  eqok ser(fffff), "+ + + + sin sin sin sin sin", "Deep a f"

  f = Function\combine(Operator.Add, Function.sin\copy!, Function\combine(Operator.Multiply, Function.cos\copy!, Function.tan\copy!))
  eqok ser(f), "+ sin * cos tan", "An example"

test "Function modifiers", ->
  id = Function.id\copy!
  id.yStretch = 11
  eqok ser(id), "(11,)id", "yStretch"

  id = Function.id\copy!
  id.yOffset = 11
  eqok ser(id), "(,11)id", "yOffset"

  id = Function.id\copy!
  id.xStretch = 11
  eqok ser(id), "id(11,)", "xStretch"

  id = Function.id\copy!
  id.xOffset = 11
  eqok ser(id), "id(,11)", "xOffset"

  id = Function.id\copy!
  id.yStretch = 11
  id.yOffset = 12
  id.xStretch = 13
  id.xOffset = 14
  eqok ser(id), "(11,12)id(13,14)", "All"

test "Op modifiers", ->
  f = Function.sin\copy!
  f.yStretch = 15
  f.yOffset = 16
  f.xStretch = 17
  f.xOffset = 18
  g = Function.cos\copy!
  g.yStretch = 19
  g.yOffset = 20
  g.xStretch = 21
  g.xOffset = 22
  fg = Function\combine(Operator.Multiply, f, g)
  fg.yStretch = 11
  fg.yOffset = 12
  fg.xStretch = 13
  fg.xOffset = 14
  expect = "(11,12)*(13,14) (15,16)sin(17,18) (19,20)cos(21,22)"
  eqok ser(fg), expect, "Operator with modifiers"

  f = Function.sin\copy!
  g = Function.cos\copy!
  g.yStretch = 15
  h = Function.tan\copy!
  fg = Function\combine(Operator.Subtract, f, g)
  fg.yStretch = 11
  fg.yOffset = 12
  fg.xStretch = 13
  fg.xOffset = 14
  fgh = Function\combine(Operator.Add, fg, h)
  eqok ser(fgh), "+ (11,12)-(13,14) sin (15,)cos tan", "Nested modifiers"

