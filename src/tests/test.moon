testData = require "lib/gambiarra"

print("Util")
require "tests/testutil"

print("Point")
require "tests/testpoint"

print("Camera")
require "tests/testcamera"

print("Function")
require "tests/testfunction"

print("Parse Functions")
require "tests/testparsefunction"

print "Serialize Functions"
require "tests/testserializefunction"

print("OnKey")
require "tests/testonkey"

print("Model")
require "tests/testmodel"

print("Level")
require "tests/testlevel"

print("SerializeLevel")
require "tests/testserializelevel"

print("Detector")
require "tests/testdetector"

print "Controller"
require "tests/testcontroller"

print "Loadlevel"
require "tests/testloadlevel"

print "Hint"
require "tests/testhint"

print(testData\report())
