
test = require "lib/gambiarra"
Point = require "model/point"
Detector = require "model/detector"

test "Default Constructor", ->
  d = Detector()
  eqok d.pos, Point(0, 0), "pos"
  eqok d.shouldOverlap, true, "shouldOverlap"
  eqok d.overlapped, false, "overlapped"
  eqok d.span, 0.1, "span"

test "Constructor", ->
  p = Point(13, 37)
  d = Detector Point(13, 37), false, 17
  eqok d.pos, p, "pos"
  eqok d.shouldOverlap, false, "shouldOverlap"
  eqok d.overlapped, false, "overlapped"
  eqok d.span, 17, "span"

test "Overlapping", ->
  d = Detector Point(0, 0), true, 1
  ok not d.overlapped, "Not overlapped to start with"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->x}}
  ok d.overlapped, "clearly overlapping function"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->2}}
  ok not d.overlapped, "clearly missing function"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->1}}
  ok d.overlapped, "upper edge function"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->(-1)}}
  ok d.overlapped, "lower edge function"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {}}
  ok not d.overlapped, "no function"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->x}}
  d\update {functions: {}}
  ok not d.overlapped, "Stops overlapping with no function"

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->x}}
  d\update {functions: {(x)->2}}
  ok not d.overlapped, "Stops overlapping with missing function"

test "Satisfied", ->
  d = Detector Point(0, 0), true, 1
  ok not d\isSatisfied!

  d = Detector Point(0, 0), false, 1
  ok d\isSatisfied!

  d = Detector Point(0, 0), true, 1
  d\update {functions: {(x)->x}}
  ok d\isSatisfied!

  d = Detector Point(0, 0), false, 1
  d\update {functions: {(x)->x}}
  ok not d\isSatisfied!
  return
  
test "Equality", ->
  ok Detector! == Detector!, "Empty cons"
  ok Detector(Point(0, 0), true, 0.1) == Detector!, "Default vs empty"
  ok Detector! == Detector(Point(0, 0), true, 0.1), "emtpy vs default"
  ok Detector(Point(0, 0), true, 0.1) == Detector(Point(0, 0), true, 0.1), "default"
  ok Detector(Point(1, 3), false, .3) == Detector(Point(1, 3), false, .3), "custom values"
  ok Detector(Point(2, 3), false, .3) != Detector(Point(1, 3), false, .3), "Different point"
  ok Detector(Point(1, 3), true, .3) != Detector(Point(1, 3), false, .3), "Different bool"
  ok Detector(Point(1, 3), false, -1) != Detector(Point(1, 3), false, .3), "different span"
  d1 = Detector!
  d2 = Detector!
  d1.overlapped = false
  d2.overlapped = true
  ok d1 == d2, "Different overlapped should still be equal"


