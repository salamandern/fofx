moon = require "moon"
pl = require("pl.import_into")()
YAML = require "yaml"
fun = require "fun"
Util = require "util/util"
Level = require "model/level"
Function = require "model/function"
Detector = require "model/detector"
Point = require "model/point"
serf = require "util/serializefunction"
parf = require "util/parsefunction"

serialize = (f) ->
  spec = {
    functions: Util.map(serf, f.functions),
    detectors: f.detectors,
    functionBank: Util.map(serf, f.functionBank),
  }
  ret = YAML.dump(spec)
  ret

makeDetector = (spec) ->
  pos = Point(spec.pos.x, spec.pos.y)
  Detector(pos, spec.shouldOverlap, spec.span)

isValidSpec = (spec) ->
  type(spec) == "table" and
  type(spec.functions) == "table" and
  type(spec.detectors) == "table" and
  type(spec.functionBank) == "table"

deserialize = (str) ->
  if type(str) == "string"
    spec = YAML.load(str)
    if isValidSpec(spec)
      spec.functions = pl.tablex.imap(parf, spec.functions)
      spec.detectors = pl.tablex.imap(makeDetector, spec.detectors)
      spec.functionBank = pl.tablex.imap(parf, spec.functionBank)
      Level.fromSpec(spec)
    else
      nil
  else
    nil

return {
  :serialize,
  :deserialize,
}
