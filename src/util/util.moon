pl = (require "pl.import_into")!

isPrefix = (string, prefix) ->
  return string.sub(string, 1, string.len(prefix)) == prefix

isSuffix = (string, suffix) ->
  return suffix =='' or string.sub(string,-string.len(suffix)) == suffix

isYamlFile = (fileName) ->
  return isSuffix(fileName, ".yaml") or isSuffix(fileName, ".yml")

shallowCopyKeepMt = (orig) ->
  orig_type = type(orig)
  copy = nil
  if orig_type == 'table'
    copy = {}
    for orig_key, orig_value in pairs(orig)
      copy[orig_key] = orig_value
    setmetatable(copy, getmetatable(orig))
  else -- number, string, boolean, etc
    copy = orig
  return copy

deepCopyKeepMt = (orig) ->
  orig_type = type(orig)
  copy = nil
  if orig_type == 'table' and not orig._dontcopy
    copy = {}
    for orig_key, orig_value in next, orig, nil
      copy[deepCopyKeepMt(orig_key)] = deepCopyKeepMt(orig_value)
    setmetatable(copy, getmetatable(orig))
  else -- number, string, boolean, etc
    copy = orig
  return copy

shallowEqualArrays = (as, bs) ->
  pl.tablex.compare(as, bs, pl.operator.eq)

forEach = (array, f) ->
  for _, v in ipairs(array)
    f(v)

map = (func, array) ->
  new_array = {}
  for i,v in ipairs(array)
    new_array[i] = func(v)
  return new_array

empty = (arr) ->
  #arr == 0

evalWith = (x) ->
  return (f) -> f(x)

compose = (f, g) ->
  return (x) -> f(g(x))

do_if_number = (subject, callback) ->
  num = tonumber(subject)
  if num ~= nil
    callback(num)

overrideEntries = (subject, entries) ->
  for k,v in pairs(entries)
    subject[k] = v

createImage = (w, h, col) ->
  data = love.image.newImageData(w, h)
  for x = 0, w-1
    for y = 0, h-1
      data\setPixel(x, y, unpack(col))
  return love.graphics.newImage(data)

between = (a, x, b) ->
  return (a <= x and x <= b) or (a >= x and x >= b)

randcolor = () -> {
  math.random(50, 255),
  math.random(50, 255),
  math.random(50, 255),
  255,
}

-- Never modify a color directly - create a new one.
-- They are used by reference so modifying could fuck things up for everyone.
col = {
  red: {255, 0, 0, 255},
  green: {0, 255, 0, 255},
  blue: {0, 0, 255, 255},
  brown: {255, 255, 0, 255},
  purple: {255, 0, 255, 255},
  cyan: {0, 255, 255, 255},
  white: {255, 255, 255, 255},
  black: {0, 0, 0, 255},
  lightgray: {200, 200, 200, 255},
  gray: {120, 120, 120, 255},
  darkgray: {50, 50, 50, 255},
}

Util = {
  :shallowCopyKeepMt,
  :deepCopyKeepMt,
  :isPrefix,
  :isSuffix,
  :isYamlFile,
  :shallowEqualArrays,
  :forEach,
  :map,
  :empty,
  :evalWith,
  :compose,
  :do_if_number,
  :overrideEntries,
  :createImage,
  :between,
  :randcolor
  :col,
}

return Util
