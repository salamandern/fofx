lpeg = require "lpeg"
Function = require "model/function"
Operator = require "model/operator"
Util = require "util/util"
moon = require "moon"

integer = lpeg.R("09")^1
float1 = lpeg.P("-")^-1 * integer * (lpeg.P(".") * integer)^-1
fl1 = float1 / tonumber

minus = lpeg.P("-")
integer = lpeg.R("09")^1
dot = lpeg.P(".")
minus = lpeg.P("-")
float_str = minus^-1 * ((integer * dot * integer) + (dot * integer) + integer)
float = float_str / tonumber
optfloat =  float^-1
float_or_0 = float + lpeg.Cc(0)
float_or_1 = float + lpeg.Cc(1)

en = -lpeg.P(1)
ws = lpeg.P(" ")
wso = ws^0
comma = wso * lpeg.P(",") * wso
lparen = lpeg.P("(") * wso
rparen = wso * lpeg.P(")")
tuple_capt = lpeg.Ct(lparen * float_or_1 * comma * float_or_0 * rparen) + lpeg.Cc({1, 0})

function_name_str = lpeg.P("id") + "sin" + "cos" + "tan" + "asin" + "acos" + "atan" + "abs" + "ceil" + "floor" + "log10" + "log" + "sqrt"
function_name = function_name_str / ((nm) -> {"fun", nm})
op_name_str = lpeg.S("+-*/o")
op_name = op_name_str / ((op) -> {"op", op})

tokenobj = (arr) -> {
  type: arr[2][1],
  name: arr[2][2],
  yStretch: arr[1][1],
  yOffset: arr[1][2],
  xStretch: arr[3][1],
  xOffset: arr[3][2],
}

func = tuple_capt * function_name * tuple_capt
func = lpeg.Ct(func) / tokenobj
operator = tuple_capt * op_name * tuple_capt
operator = lpeg.Ct(operator) / tokenobj
token = func + operator

pat = lpeg.P({
  "exp",
  exp: lpeg.Ct(func) + lpeg.V("op"),
  op: lpeg.Ct(operator * ws * lpeg.V("exp") * ws * lpeg.V("exp")),
})

--op_or_func = func + (operator * ws * op_or_func * ws * op_or_func)


parseTokens = (tokens) ->
  curr = tokens[1]
  ret = switch curr.type
    when "fun"
      ret = Function[curr.name]\copy!
      ret
    when "op"
      assert curr.type == "op"
      opObj = switch curr.name
        when "+"
          Operator.Add
        when "-"
          Operator.Subtract
        when "*"
          Operator.Multiply
        when "/"
          Operator.Divide
        when "o"
          Operator.Compose
        else
          error "Bad op name" .. curr.name

      ret = Function\combine opObj,
        parseTokens tokens[2],
        parseTokens tokens[3]
      ret
    else
      error "Bad token type " .. curr.type
  ret.yStretch = curr.yStretch
  ret.yOffset = curr.yOffset
  ret.xStretch = curr.xStretch
  ret.xOffset = curr.xOffset
  return ret

return (str) ->
  tokens = (wso * pat * wso * en)\match(str)
  if nil != tokens
    ret, res = pcall parseTokens, tokens
    if ret
      return res
    else
      print res
      return nil
  return nil
