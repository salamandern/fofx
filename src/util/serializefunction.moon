tuple = (s, o) ->
  if s == 1 and o == 0
    return ""
  elseif s == 1
    return "(,#{o})"
  elseif o == 0
    return "(#{s},)"
  else
    return "(#{s},#{o})"

tokenize = (t) ->
  nm = t.operator and t.operator.name or t.name
  return tuple(t.yStretch, t.yOffset) .. nm .. tuple(t.xStretch, t.xOffset)

serialize = (f) ->
  op = f.operator
  if op
    return "#{tokenize(f)} #{serialize(f.parts[1])} #{serialize(f.parts[2])}"
  else
    return tokenize(f)

return serialize
