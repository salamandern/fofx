Point = require "model/point"

class Camera
  new: (x, y) =>
    @pos = Point(x, y)
    @hdim = Point(2.8, 2)

  worldToScreen: (p) =>
    scale = @coordinateScale()
    postTranslate = Point(love.graphics.getDimensions())/2
    return (p - @pos)*scale + postTranslate

  screenToWorld: (p) =>
    scale = @coordinateScale()
    postTranslate = Point(love.graphics.getDimensions())/2
    return (p - postTranslate)/scale + @pos

  coordinateScale: () =>
    xScale =  love.graphics.getWidth() / (2*@hdim.x)
    yScale = -love.graphics.getHeight() / (2*@hdim.y)
    return Point(xScale, yScale)

return Camera
