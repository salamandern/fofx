
class Hint
  new: (@text = "", @duration = 0) =>
    @timeLeft = @duration

  __eq: (other) =>
    (@text == other.text) and
    (@duration == other.duration)
return Hint
