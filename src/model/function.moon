Util = require "util/util"
Operator = require "model/operator"

-- A Function tries to have the properties that the mathematical concept has,
-- but we'll see how far that goes.
-- Right now, it can be given an X value and will output a Y value.
class Function
  @f_id = (x) -> x

  new: (@f = Function.f_id, @name = "id", @color = {255, 255, 255}) =>
    @xOffset = 0
    @yOffset = 0
    @xStretch = 1
    @yStretch = 1
    -- Combining
    @parts = {}
    @operator = nil

  __call: (x) =>
    x = x * @xStretch - @xOffset
    fofx = nil
    if @operator
      assert(@operator.nOperands == #@parts) -- can optmize away
      partYs = Util.map(Util.evalWith(x), @parts)
      fofx = @operator.evaluate(x, unpack(@parts))
    else
      fofx = @.f(x)
    y = fofx * @yStretch + @yOffset
    return y

  __eq: (other) =>
    (@name == other.name) and
    (@f == other.f) and
    (@xOffset == other.xOffset) and
    (@yOffset == other.yOffset) and
    (@xStretch == other.xStretch) and
    (@yStretch == other.yStretch) and
    (@operator == other.operator) and
    Util.shallowEqualArrays(@parts, other.parts)

  copy: (o = {}) =>
    result = Util.shallowCopyKeepMt(@)
    Util.overrideEntries(result, o)
    return result

  combine: (op, f, g) =>
    result = @@id\copy()
    result.operator = op
    assert((op.nOperands == 1) == (g == nil), "Function\\combine called with bad function amount")
    result.parts = g and {f, g} or {f}
    return result

-- Math fs
Function.__base.id = Function(Function.f_id, "id")
Function.__base.sin = Function(math.sin, "sin")
Function.__base.cos = Function(math.cos, "cos")
Function.__base.tan = Function(math.tan, "tan")
Function.__base.asin = Function(math.asin, "asin")
Function.__base.acos = Function(math.acos, "acos")
Function.__base.atan = Function(math.atan, "atan")
Function.__base.abs = Function(math.abs, "abs")
Function.__base.ceil = Function(math.ceil, "ceil")
Function.__base.floor = Function(math.floor, "floor")
Function.__base.log = Function(math.log, "log")
Function.__base.log10 = Function(math.log10, "log10")
Function.__base.sqrt = Function(math.sqrt, "sqrt")
Function.__base.cool = Function\combine(Operator.Add, Function.sin\copy(), Function.id\copy())

return Function
