
class Operator

  new: (@name, @evaluate, @nOperands) =>
    @_dontcopy = true

  paddedName: =>
    return " " .. @name .. " "

  serialize: (parts) =>
    if @nOperands == 2
      assert(#parts == 2)
      return ("(" .. parts[1]\serialize() .. ")" ..
        @paddedName() ..
        "(" .. parts[2]\serialize() .. ")")

Operator.__base.Add = Operator("+", ((x, a, b) -> a(x) + b(x)), 2)
Operator.__base.Subtract = Operator("-", ((x, a, b) -> a(x) - b(x)), 2)
Operator.__base.Multiply = Operator("*",  ((x, a, b) -> a(x) * b(x)), 2)
Operator.__base.Divide = Operator("/",  ((x, a, b) -> a(x) / b(x)), 2)
Operator.__base.Compose = Operator("o",  ((x, a, b) -> a(b(x))), 2)

return Operator
