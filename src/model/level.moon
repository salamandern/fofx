Util = require "util/util"
Hint = require "model/hint"

local allLevels

class Level
  new: =>
    @functions = {}
    @detectors = {}
    @functionBank = {}
    @hint = Hint!
    @stretchEnabled = true

  fromSpec: (spec) ->
    ret = Level!
    assert spec
    assert spec.functions
    assert spec.detectors
    assert spec.functionBank
    ret.functions = Util.deepCopyKeepMt(spec.functions)
    ret.detectors = Util.deepCopyKeepMt(spec.detectors)
    ret.functionBank = Util.deepCopyKeepMt(spec.functionBank)
    if spec.hint
      ret.hint = Hint spec.hint.text, spec.hint.duration
    if not (spec.stretchEnabled == nil)
      ret.stretchEnabled = spec.stretchEnabled
    return ret

  __eq: (other) =>
    return Util.shallowEqualArrays(@functions, other.functions) and
      Util.shallowEqualArrays(@detectors, other.detectors) and
      Util.shallowEqualArrays(@functionBank, other.functionBank)

return Level

