Util = require "util/util"


class Point
  new: (@x = 0, @y = 0) =>

  __eq: (other) => @x == other.x and @y == other.y
  __unm: () => Point(-@x, -@y)
  __tostring: () =>
    "(" .. @x .. "," .. @y .. ")"
  __inherited: (cls) =>
    cls.__base.__add = @__add
    cls.__base.__sub = @__sub
    cls.__base.__mul = @__mul
    cls.__base.__div = @__div
    cls.__base.__unm = @__unm
    cls.__base.__tostring = @__tostring

  coords: () => @x, @y
  inside: (a, b) =>
      Util.between(a.x, @x, b.x) and Util.between(a.x, @x, b.x)

opfunct = (op) ->
  (a, b) ->
    if type(a) == "table"
      if type(b) == "table"
        Point(op(a.x, b.x), op(a.y, b.y))
      else
        Point(op(a.x, b), op(a.y, b))
    else
      Point(op(a, b.x), op(a, b.y))


Point.__base.__add = opfunct((a, b) -> a + b)
Point.__base.__sub = opfunct((a, b) -> a - b)
Point.__base.__mul = opfunct((a, b) -> a * b)
Point.__base.__div = opfunct((a, b) -> a / b)


return Point
