Point = require "model/point"

class Detector extends Point

  new: (@pos = Point(0, 0), @shouldOverlap = true, @span = 0.1) =>
    @overlapped = false

  update: (world) =>
    @overlapped = false
    for _, f in ipairs(world.functions) do
      if math.abs(@pos.y - f(@pos.x)) <= @span then
        @overlapped = true
        break

  isSatisfied: =>
    return @overlapped == @shouldOverlap

  __eq: (other) =>
    @pos == other.pos and
    @shouldOverlap == other.shouldOverlap and
    @span == other.span

return Detector
