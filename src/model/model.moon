Util = require "util/util"
Camera = require "model/camera"

class Model
  new: (level = {}) =>
    @time = 0
    @functions = {}
    @currentFunction = 1
    @camera = Camera()
    @detectors = {}
    @adapt = "xy"
    @isAdapting = false
    @levelMetadata = nil
    @winTimer = nil
    @shouldChangeToNext = false
    @locked = false
    @register1 = -1
    @register2 = -1
    Util.overrideEntries(@, level)

  isValidFunctionId: (id) =>
    return id > 0 and id <= #@functions

return Model
