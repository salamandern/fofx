moon = require "moon"
Util = require "util/util"
OnKeyLib = require "controller/onkey"
Level = require "model/level"
Function = require "model/function"
Operator = require "model/operator"
Point = require "model/point"
GUILib = require "view/gui"
runAfter = require "controller/runafter"
serializeLevel = (require "util/serializelevel").serialize

mouse = love.mouse

class Controller
  winTime: 2.5

  new: (newWorld) =>
    @world = newWorld
    @GUI = GUILib(@world, @)
    @OnKey = OnKeyLib(@)
    @savedMousePos = nil
    @resetFunctionMetadata(@)
    onNumber = (n) => @setCurrentFunction(tonumber(n))
    @OnKey\add({
      "1": onNumber,
      "2": onNumber,
      "3": onNumber,
      "4": onNumber,
      "5": onNumber,
      "6": onNumber,
      "7": onNumber,
      "8": onNumber,
      "9": onNumber,
      x: @switchAdaptationStyle,
      l: @winLevel,
      t: @setWinTimer,
      a: @addRandomFunction,
      r: @removeLastFunction,
    })

    @initWorld!

  update: (dt) =>
    @GUI\update()
    @updateMouseState()
    @updateDetectors()
    @checkWinState()
    @updateWinClock(dt)
    @updateHintClock!

  initWorld: () =>
    if @world.hint
      @world.hintTimer = runAfter(@world.hint.duration, () -> @removeHint!)

  removeHint: () =>
    @world.hint = nil
    @world.hintTimer = nil

  updateWinClock: (dt) =>
    if @world.winTimer
      @world.winTimer()

  updateHintClock: () =>
    if @world.hintTimer
      @world.hintTimer!

  updateMouseState: =>
    @updateMouseRelativeMode()

  updateMouseRelativeMode: =>
    mouseRel = mouse.getRelativeMode()
    if @world.isAdapting
      if not mouseRel
        mouse.setRelativeMode(true)
        x, y = mouse.getPosition()
        @savedMousePos = {x, y}
    else
      if mouseRel then
        mouse.setRelativeMode(false)
        if @savedMousePos then
          mouse.setPosition(unpack(@savedMousePos))

  updateDetectors: =>
    for _, d in ipairs(@world.detectors)
      d\update(@world)

  addNewFunction: (f) =>
    f.color = Util.randcolor()
    table.insert(@world.functions, f)
    @resetFunctionMetadata()

  addRandomFunction: =>
    newF = Function.sin\copy({yOffset: math.random()})
    @addNewFunction(newF)

  addCombinationFunction: (op) =>
    assert (moon.type(op) == Operator), "op should be and operator"
    if @world\isValidFunctionId(@world.register1) and @world\isValidFunctionId(@world.register2)
      a1 = @world.functions[@world.register1]
      a2 = @world.functions[@world.register2]
      newF = Function\combine(op, a1, a2)
      @addNewFunction(newF)

  removeLastFunction: =>
    table.remove(@world.functions)
    @resetFunctionMetadata()

  setCurrentFunction: (id) =>
    if 1 <= id and id <= #@world.functions
      @world.currentFunction = id

  checkWinState: () =>
    allCorrect = true
    for _, d in ipairs(@world.detectors)
      if not d\isSatisfied()
        allCorrect = false
        break
    if allCorrect and not @world.winTimer and not @world.isAdapting
      @setWinTimer()

  setWinTimer: () =>
    @world.winTimer = runAfter(@winTime, () -> @winLevel())
    @world.locked = true
    @world.isAdapting = false

  winLevel: () =>
    print("WOHOOW")
    @world.locked = false
    @world.winTimer = nil
    @world.shouldChangeToNext = true

  resetFunctionMetadata: () =>
    nFunctions = #@world.functions
    if @world.currentFunction > nFunctions
      @world.currentFunction = nFunctions
    if @world.currentFunction < 1
      @world.currentFunction = 1
    @world.register1 = -1
    @world.register2 = -1

  switchAdaptationStyle: () =>
    if @world.adapt == "xy" and @world.stretchEnabled
      @world.adapt = "stretch"
    else
      @world.adapt = "xy"

  dumpLevelToFile: (name) =>
    data = "-----------\n" .. @dumpLevel!
    filename = "#{name}.fworld"
    success, err = love.filesystem.append(filename, data)
    if not success
      print "WARNING: could not dump world to #{filename}. Error: #{err}"

  dumpLevel: =>
    serializeLevel @world

  keypressed: (key, scancode, isRepeat) =>
    @OnKey\run(scancode)

  mousemoved: (x, y, dx, dy) =>
    if @world.isAdapting
      worldDelta = Point(dx, dy)/@world.camera\coordinateScale()
      f = @world.functions[@world.currentFunction]
      if f then
        if @world.adapt == "xy"
          f.yOffset = f.yOffset + worldDelta.y
          f.xOffset = f.xOffset + worldDelta.x
        else
          f.xStretch = f.xStretch + worldDelta.x
          f.yStretch = f.yStretch + worldDelta.y

  mousepressed: () =>
    if not (@GUI.hasMouse or @world.locked) then
      @world.isAdapting = true

  mousereleased: () =>
    @world.isAdapting = false

return Controller
