(runAfterSecs, callback) ->
  startTime = os.time()
  updateClock = ->
    timediff = os.difftime(os.time(), startTime)
    done = timediff >= runAfterSecs
    if done and callback
      print("Calling back!")
      callback()
      callback = nil
    return done
  return updateClock
