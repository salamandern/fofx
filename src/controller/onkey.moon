Util = require "util/util"

class OnKey
  new: (subject) =>
    @actions = {}
    @subject = subject

  add: (actions) =>
    Util.overrideEntries(@actions, actions)

  run: (id) =>
    action = @actions[id]
    if action
        if @subject
            action(@subject, id)
        else
            action(id)

return OnKey
