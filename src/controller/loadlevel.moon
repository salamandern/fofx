pl = (require "pl.import_into")!
moon = require "moon"
Util = require "util/util"

Level = require "model/level"
deserl = (require "util/serializelevel").deserialize
lf = love.filesystem
--LEVELS_DIR = "levels"
LEVELS_DIR = "containingFolder/levels"

local *

availableLevels = ->
  return #sortedAvailableLevelNames!

sortedAvailableLevelNames = ->
  --fusedItems = lf.getDirectoryItems("levels")
  containingFolderItems = lf.getDirectoryItems(LEVELS_DIR)
  lvlNames = pl.tablex.filter containingFolderItems, validLevelName
  table.sort lvlNames
  lvlNames

validLevelName = (levelName) ->
  return (not (lf.isDirectory levelName)) and Util.isYamlFile levelName

serve = (id) ->
  lvlNames = sortedAvailableLevelNames!
  if id <= 0 or #lvlNames < id
    return nil
  else
    lvlName = lvlNames[id]
    strLevel = lf.read "#{LEVELS_DIR}/#{lvlName}"
    ret = deserl strLevel
    ret.levelMetadata = {
      id: id
    }
    return ret

return {
  :availableLevels,
  :serve,
}
