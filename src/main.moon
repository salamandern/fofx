package.cpath = "./?.dll;./libs/?.dll;"

require "lib/strict"

moon = require "moon"
suit = require "lib/SUIT"
Model = require "model/model"
View = require "view/view"
Controller = require "controller/controller"
Level = require "model/level"
loadlevel = require "controller/loadlevel"

kb = love.keyboard
lg = love.graphics
lf = love.filesystem

world = nil
controller = nil
local changeToNextLevel
local newLevel
local initContainingFolder

love.load = ->
  initContainingFolder!
  newLevel(1)
  print "Saving all files to #{love.filesystem.getSaveDirectory!}"
  print "Path for looking for libs: #{package.cpath}"

love.update = (dt) ->
  world.time += dt
  controller\update(dt)
  if world.shouldChangeToNext
    changeToNextLevel()

love.draw = () ->
  View.render(world)
  lg.setColor(255, 255, 255)
  suit.draw()

love.textinput = (t) ->
  suit.textinput(t)

love.keypressed = (key, scancode, isRepeat) ->
  if scancode == "escape"
    love.event.quit()
  controller\keypressed(key, scancode, isRepeat)
  suit.keypressed(key)

love.mousemoved = (x, y, dx, dy) ->
  controller\mousemoved(x, y, dx, dy)

love.mousepressed = () ->
  controller\mousepressed()

love.mousereleased = () ->
  controller\mousereleased()

changeToNextLevel = () ->
  nextLevelId = (world.levelMetadata.id%loadlevel.availableLevels!) + 1
  newLevel(nextLevelId)

newLevel = (id) ->
  world = Model(loadlevel.serve(id))
  controller = Controller(world)

initContainingFolder = () ->
  print "Gonna see if I can init the containing folder"
  if not lf.isFused()
    print "Cannot init containing folder, not running in fused mode!"
    return

  containingFolderPath = lf.getSourceBaseDirectory!
  if not containingFolderPath
    print "Could not get containing forlder path"
    return

  lf.mount containingFolderPath, "containingFolder"
  print "Initting containint folder"
  return

