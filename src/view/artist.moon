Util = require "util/util"

plainImagePool = {}
selectedImagePool = {}
depImagePool = {}
buttonSize = 40

local *

fButtonImage = (col) ->
  fromPool = plainImagePool[col]
  if fromPool ~= nil
    return fromPool
  else
    img = Util.createImage(buttonSize, buttonSize, col)
    plainImagePool[col] = img
    return img

fSelectedButtonImage = (col) ->
  fromPool = selectedImagePool[col]
  if fromPool ~= nil
    return fromPool
  else
    img = createBorderImage(col, Util.col.white, 4)
    selectedImagePool[col] = img
    return img

fDepButtonImage = (col) ->
  fromPool = depImagePool[col]
  if fromPool ~= nil
    return fromPool
  else
    img = createBorderImage(col, Util.col.white, 2)
    depImagePool[col] = img
    return img

createBorderImage = (col, borderCol, border) ->
  border = border or 3
  img = Util.createImage(buttonSize, buttonSize, borderCol)
  data = img\getData()
  for x = 0, buttonSize-1
    for y = 0, buttonSize-border-1
      data\setPixel(x, y, unpack(col))
  img\refresh()
  return img

Artist = {
  :fButtonImage,
  :fSelectedButtonImage,
  :fDepButtonImage,
  :createBorderImage,
}

return Artist
