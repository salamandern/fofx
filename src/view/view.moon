Util = require "util/util"
Point = require "model/point"
lg = love.graphics

-- Draw invariants - updated every frame
world = nil -- Set each render cycle, not to have to pass as param everywhere.
font = nil -- Font
scrMid = nil -- Point
scrDim = nil -- Point
cam = nil -- Camera
camScale = nil -- Point
-- Magic numbers
notchH = 5

local *

render = (currentWorld) -> -- World is drawn with positive axis up
  updateDrawInvariants(currentWorld)
  drawBackground()
  drawWorld()
  drawStatus()

drawWorld = ->
  lg.push()
  makeStuffPixelPerfect() -- Good for math, bad for text
  drawFunctions()
  drawDetectors()
  lg.pop()

updateDrawInvariants = (currentWorld) ->
  world = currentWorld
  font = lg.getFont()
  scrDim = Point(lg.getWidth(), lg.getHeight())
  scrMid = scrDim/2
  cam = currentWorld.camera
  camScale = cam\coordinateScale()

drawBackground = () ->
  drawDebugPoints()
  drawAxes()

drawAxes = () ->
  lg.setLineStyle("rough")
  lg.setColor(Util.col.darkgray)
  lg.setLine(100, "rough")
  -- axis lines
  lg.line(0, scrMid.y, scrDim.x, scrMid.y)
  lg.line(scrMid.x, 0, scrMid.x, scrDim.y)
  -- numbers
  lg.setColor(Util.col.gray)
  for i = 0.5, math.max(cam.hdim\coords!), 0.5
    drawNotch( i, true)
    drawNotch(-i, true)
    drawNotch( i, false)
    drawNotch(-i, false)

-- val in math scale
drawNotch = (val, horizontal) ->
  a, b, scrVal = nil, nil, nil
  if horizontal
    scrVal = val * camScale.x
    a = Point(scrVal, 0)
    b = Point(scrVal, notchH)
  else
    scrVal = val * camScale.y
    a = Point(0,      scrVal)
    b = Point(notchH, scrVal)
  a = a + scrMid
  b = b + scrMid

  lg.setColor(Util.col.darkgray)
  drawLine(a, b)
  lg.setColor(Util.col.gray)
  lg.print(tostring(val), b.x, b.y)

drawDetectors = () ->
  for _, d in pairs(world.detectors)
    lg.setColor(d\isSatisfied() and Util.col.green or Util.col.red)
    spanDelta = Point(0, d.span)
    a = world.camera\worldToScreen(d.pos + spanDelta)
    b = world.camera\worldToScreen(d.pos - spanDelta)
    lg.circle("fill", a.x, a.y, 3, 10)
    lg.circle("fill", b.x, b.y, 3, 10)
    drawLine(a, b)

drawFunctions = () ->
  love.graphics.push()

  lg.setLineStyle("smooth")
  lg.setLineWidth(100000000) -- Wtf this does nothing
  lg.setLineJoin("bevel")
  
  transformToFunctionPlane()
  Util.forEach(world.functions, drawFunction)
  --drawDebugPoints()

  love.graphics.pop()

drawStatus = ->
  if world.winTimer
    drawWinText!
  drawHint!

drawWinText = ->
  lg.setColor(Util.col.green)
  lg.setLineWidth(1)
  lg.print("You beat the level, congrats!", 100, 100)

drawHint = ->
  if world.hint
    lg.setColor(Util.col.white)
    lg.setLineWidth(1)
    lg.print(world.hint.text, 100, 70)

drawFunction = (f) ->
  lg.setColor(f.color)
  resolution = lg.getWidth()
  x0 = cam.pos.x - cam.hdim.x
  interval = cam.hdim.x*2
  step = interval/resolution
  pts = {}
  for x=x0, x0+interval, step
    table.insert(pts, x)
    table.insert(pts, f(x))



  love.graphics.line(pts)

drawDebugPoints = () ->
  lg.setColor(Util.col.white)
  lg.setPointSize(3)
  lg.points({
    -- Grid
    {-1,  1}, {0,  1}, {1,  1},
    {-1,  0}, {0,  0}, {1,  0},
    {-1, -1}, {0, -1}, {1, -1},
    --Upper right corner marker
    {0.7, 1}, {0.9, 1},
                        {1, 0.7},
                        {1, 0.9}
  })

drawLine = (a, b) ->
  lg.line(a.x, a.y, b.x, b.y)

-- Note, gl applies this in reverse order.
-- You should alreaday know this, but so should I have.
transformToFunctionPlane = () ->
  lg.translate(scrMid\coords()) -- Because screen 0 is in topleft corner
  lg.scale(camScale\coords()) -- Scale by camera scale
  lg.translate((-world.camera.pos)\coords()) -- Move to camera position

-- This function cleanly amends two things that otherwise cause artefacts in
-- rendering.
--  TL;DR: If you use this function, pixels start from (1,1) and you don't have
--  to offset them by 0.5. But text looks bad.
-- 1: Lua counts from 1, so many point drawings on arrays leave a line of pixels
--    blank to the left. Could counteract it everywhere, but let's instead play
--    by Lua's rules and let pixel indices start at 1.
-- 2. Pixel positions are actually in their centers, so drawing (1,1) draws the
--    point in-between four pixels. Drawing (0.5, 0.5) is what fills in the
--    top-left pixel, but again, let's put that here and not have to think about
--    it.
-- However, this also fucks text up really bad.
-- Usage: Use at absolute top level of rendering stack.
makeStuffPixelPerfect = () ->
  lg.translate(-0.5, -0.5)

View = {
  :render,
  :updateDrawInvariants,
  :drawBackground,
  :drawAxes,
  :drawNotch,
  :drawDetectors,
  :drawFunctions,
  :drawStatus,
  :drawFunction,
  :drawDebugPoints,
  :drawLine,
  :transformToFunctionPlane,
  :makeStuffPixelPerfect,
}

return View
