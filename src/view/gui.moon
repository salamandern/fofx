suit = require "lib/SUIT"
Artist = require "view/artist"
Operator = require "model/operator"

class GUI
  new: (@world, @controller) =>
    @hasMouse = false

  update: (dt) =>
    @hasMouse = false
    if not @world.isAdapting
      @functionButtons()
      @adaptButtons()
      @functionBankGUI()
      @registerButtons()
      @combinationButtons()
      @devButtons()
    @hasMouse = suit\anyHovered()

  fDependsOn: (f, g)  =>
    for _, p in ipairs(f.parts)
      if p == g or GUI\fDependsOn(p, g)
        return true
    return false

  functionButtons: () =>
    noteCurrentFunction = 1 < #@world.functions
    cf = @world.functions[@world.currentFunction]
    for i, f in ipairs(@world.functions)
      x=60 * i - 40
      y=540

      img = Artist.fButtonImage(f.color or Util.col.white)
      if noteCurrentFunction
        if i == @world.currentFunction
          img = Artist.fSelectedButtonImage(f.color or Util.col.white)
        elseif GUI\fDependsOn(f, cf) or GUI\fDependsOn(cf, f)
          img = Artist.fDepButtonImage(f.color or Util.col.white)

      if suit.ImageButton(img, x, y).hit
        print("Setting function to" .. i)
        @controller\setCurrentFunction(i)

  adaptButtons: () =>
    world = @world
    if world.stretchEnabled
      text = world.adapt == "stretch" and "STRETCH" or "stretch"
      if suit.Button(text, 700, 550, 40, 40).hit
        world.adapt = "stretch"
    text = world.adapt == "xy" and "MOVE" or "move"
    if suit.Button(text, 750, 550, 40, 40).hit
      world.adapt = "xy"

  functionBankGUI: () =>
    for i, f in ipairs(@world.functionBank)
      if suit.Button(f.name, 20, -30 + 50*i, 100, 30).hit
        @controller\addNewFunction(f\copy())

  registerButtons: () =>
    world = @world
    label1 = ""
    label2 = ""
    if world\isValidFunctionId(world.register1)
      label1 = tostring(world.register1)
    if world\isValidFunctionId(world.register2)
      label2 = tostring(world.register2)

    if suit.Button("1: " .. label1, 700, 10, 40, 40).hit
      world.register1 = world.currentFunction
    if suit.Button("2: " .. label2, 750, 10, 40, 40).hit
      world.register2 = world.currentFunction

  combinationButtons: () =>
    if suit.Button("+", 750, 60, 40, 40).hit
      @controller\addCombinationFunction(Operator.Add)
    if suit.Button("-", 750, 110, 40, 40).hit
      @controller\addCombinationFunction(Operator.Subtract)
    if suit.Button("*", 750, 160, 40, 40).hit
      @controller\addCombinationFunction(Operator.Multiply)
    if suit.Button("/", 750, 210, 40, 40).hit
      @controller\addCombinationFunction(Operator.Divide)
    if suit.Button("o", 750, 260, 40, 40).hit
      @controller\addCombinationFunction(Operator.Compose)

  devButtons: =>
    if suit.Button("Save level", 560, 560, 100, 30).hit
      @controller\dumpLevelToFile "dump"

return GUI
