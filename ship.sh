sh build.sh;

echo "Creating distribution zip..."
version=`./prod/inc.sh prod/version.txt`;
cd lua_build;
name=f_of_x_build$version;
zip -rq9 $name.love .;
cd ..;
mv lua_build/$name.love prod;
cd prod;
cp -r dependencies $name;
cp -r ../levels $name/levels;
sh produce.sh $name;
mv $name.exe $name;
zip -rq9 $name.zip $name;
rm -rf $name;
mv $name.zip windows;
mv $name.love lovefiles;
cd ..;

echo "Uploading to google drive..."
googlefolderid=`cat prod/googlefolderid.txt`;
gdrive sync upload prod/windows $googlefolderid;
