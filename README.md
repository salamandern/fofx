# F(x) #

A game about the beauty of mathematical functions.

![fofx.png](https://bitbucket.org/repo/gz44jX/images/3578905336-fofx.png)

CUrrently under development, working towards an MVP. 

# Setup on Linux #

You need LÖVE, Lua 5.1, Moonscript, and a bunch of lua libraries. Some are submodules of the repo, others need installing with rocks. I might make a list in the future.

After that, `moonc -t lua_build . && love lua_build` will compile and run the game.